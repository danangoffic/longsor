(function(){
    var 
        bagan_print = $("#section-to-print"),
        cache_width = bagan_print.width(),
        a4 = [595.28, 841.89];
        
        $("#cmd").click(function(){
            $("body").scrollTop(0);
            createPDF();
        });
        
        function createPDF(){
            getCanvas().then(function(canvas){
                var
                img = canvas.toDataURL("image/png"),
                doc = new jsPDF({
                    unit: 'px',
                    format: 'a4',
                    orientation: 'portrait'
                });
                doc.addImage(img, 'JPEG', 20, 20);
                doc.save('detail_goa.pdf');
                bagan_print.width(cache_width);
            });
        }
        
        function getCanvas(){
            bagan_print.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
            return html2canvas(bagan_print, {
                imageTimeout: 2000,
                removeContainer: true
            })
        }
})
