<?php
 
	date_default_timezone_set("Asia/Jakarta");	//default timezone
 	$host = "localhost";	//server host database
 	$username = "dan13";		//username database
 	$password = "dan2018";			//password database
 	$dbname = "hujan2";	//database name


	$db = new mysqli($host, $username, $password, $dbname);
	$vdata1 = (isset($_REQUEST['dataSuhuIn']) ? $_REQUEST['dataSuhuIn'] : NULL) ;	//Hasil Suhu
	// $vdata2 = "SI1";	//Id Suhu
	$vdata3 = (isset($_REQUEST['dataKelembabanRg']) ? $_REQUEST['dataKelembabanRg'] : NULL);	//Hasil Kelembaban Ruangan
	// $vdata4 = "KR1";	//Id Kelembaban Ruangan
	$vdata5 = (isset($_REQUEST['dataSuhuOut']) ? $_REQUEST['dataSuhuOut'] : NULL); //Hasil Suhu Outdoor
	// $vdata6 = "SO1"; //Id Suhu Outdoor
	$vdata7 = (isset($_REQUEST['dataKelembabanTnh']) ? $_REQUEST['dataKelembabanTnh'] : NULL); //Hasil Kelembaban Tanah
	// $vdata8 = "KT1";	//Id Kelembaban Tanah
	$vdata9 = (isset($_REQUEST['dataPres']) ? $_REQUEST['dataPres'] : NULL);	//Hasil Tekanan Udara
	// $vdata10 = "TU1";	//Id Tekanan Udara
	$vdata11 = (isset($_REQUEST['dataGas']) ? $_REQUEST['dataGas'] : NULL);	//Hasil Data Gas
	// $vdata12 = "GC1";	//Id Gas Co2
	if($vdata3 < 51){
		$curah_hujan = rand(10,50);
	}
	elseif($vdata3 < 61){
		$curah_hujan = rand(51,90);
	}
	elseif($vdata3 < 66){
		$curah_hujan = rand(91,100);
	}
	elseif($vdata3 < 71){
		$curah_hujan = rand(101, 150);
	}
	elseif($vdata3 < 76){
		$curah_hujan = rand(151, 199);
	}
	elseif($vdata3 < 80){
		$curah_hujan = rand(200, 299);
	}
	elseif ($vdata3 == 80) {
		$curah_hujan = rand(300,399);
	}
	elseif($vdata3 == 81){
		$curah_hujan = rand(400, 499);
	}
	else{
		$curah_hujan = rand(500,1500);
	}

// Check connection
	if ($db->connect_error) {
	    die("Connection failed: " . $db->connect_error);
	}else{
		require __DIR__ . '/vendor/autoload.php';
		$options = array(
		    'cluster' => 'ap1',
		    'encrypted' => true
		  );
		$pusher = new Pusher\Pusher(
			'1259986a708bccdce112',
			'5f257358a1daf99713b7',
			'520969',
			$options
		);
	$time = date("H:i:s");
	$date = date("Y-m-d");


	  // mysql_select_db($db_name1)  or die("Unable to select database");
	  //mysql_query("INSERT INTO location (latitude,longitude) VALUES ('$vdata1','$vdata2')");
	 	// 1
	 	// Insert Data Gas CO2
	 	$sql_gas = $db->query("INSERT INTO `gas`(`id_alat`, `data_gas`,  `tanggal`, `jam`) VALUES ('MQ71', '$vdata11',  '$date', '$time')");

	 	// 2
	 	// Insert Kelembaban Tanah
	 	$sql_tanah = $db->query("INSERT INTO `kelembaban_tanah`(`id_alat`, `data_kelembaban_tnh`, `tanggal`, `jam`) VALUES ('FC281', '$vdata7',   '$date', '$time')");

	 	// 3
	 	// Kelembaban Udara
	 	$sql_udara = $db->query("INSERT INTO `kelembaban_udara`(`id_alat`, `data_kelembaban_ud`, `tanggal`, `jam`) VALUES ('DHT111', '$vdata3' , '$date', '$time')");

	 	// 4
	 	// Suhu Indoor
	 	$sql_indoor = $db->query("INSERT INTO `suhu_indoor`(`id_alat`, `data_suhu_in`, `tanggal`, `jam`) VALUES ('LM351', '$vdata1' , '$date', '$time')");

	 	// 5
	 	// Suhu Outdoor
	 	$sql_outdoor = $db->query("INSERT INTO `suhu_outdoor`(`id_alat`, `data_suhu_out`,  `tanggal`, `jam`) VALUES ('DS181', '$vdata5' , '$date', '$time')");

	 	// 6
	 	// Tekanan Udara
	 	$sql_pressure = $db->query("INSERT INTO `tekanan_udara`(`id_alat`, `data_pressure`, `tanggal`, `jam`) VALUES ('BMP1801', '$vdata9' , '$date', '$time')");
		// Insert Data
	  // $sql = "INSERT INTO location (id_alat,longitude,latitude) VALUES ('$vdata1','$vdata2','$vdata3')";
	  // ==== CURAH HUJAN ==== //
	 	
	  if (($sql_gas == TRUE) &&  ($sql_tanah == TRUE) && ($sql_udara == TRUE) && ($sql_indoor == TRUE) && ($sql_outdoor == TRUE) && ($sql_pressure == TRUE)) {  	
	  	$insert_hujan = $db->query("INSERT INTO `data_hujan`(`id_alat`, `curah_hujan`, `tanggal`, `jam`, `kelembaban`, `suhu`) VALUES ('CC02','$curah_hujan','$date','$time','$vdata3','$vdata5')");
	  	// ==== NORMAL CONDITION DELETED CAUSE IT DOESN'T APPEARS ON NOTIFICATION ==== //
		// ==== CURAH HUJAN ===== //
		if($curah_hujan > 100 && $curah_hujan < 301){
	 		$kondisi_curah_hujan = 'Curah Hujan Pada Level Menengah';
	 	}elseif($curah_hujan > 300 && $curah_hujan < 501){
	 		$kondisi_curah_hujan = 'Curah Hujan Pada Level Tinggi';
	 	}elseif($curah_hujan > 500){
	 		$kondisi_curah_hujan = 'Curah Hujan Pada Level Sangat Tinggi';
	 	}

	 	// ==== GAS ==== //
	 	if($vdata11 > 80 && $vdata11 < 85){
	 		$kondisi_gas = "Kurangnya Kadar Oksigen Didalam menyebabkan lilin tidak menyala. ";
	 	}elseif($vdata11 > 84 && $vdata11 < 93){
	 		$kondisi_gas = "Kurangnya Kadar Oksigen Didalam menyebabkan hipoksia serius. ";
	 	}elseif($vdata11 > 92){
	 		$kondisi_gas = "Kurangnya Kadar Oksigen Didalam menyebabkan Kematian. ";
	 	}

	 	// ==== SUHU DALAM ==== //
	 	if(($vdata1 > 31 && $vdata1 < 37) || ($vdata1 > 21 && $vdata1 < 25)){
	 		$kondisi_suhu_dalam = "Suhu Didalam Mulai Terjadi Fluktuasi. ";
	 	}elseif(($vdata1 > 36 && $vdata1 < 42) || ($vdata1 > 17.99 && $vdata1 < 22)){
	 		$kondisi_suhu_dalam = "Suhu Didalam Mulai Terjadi Fluktuasi Tinggi. ";
	 	}elseif(($vdata1 > 41) || ($vdata1 < 18)){
	 		$kondisi_suhu_dalam = "Suhu Didalam Mulai Terjadi Fluktuasi Sangat Tinggi. ";
	 	}

	 	// ==== SUHU LUAR ==== //
	 	if(($vdata5 > 31 && $vdata5 < 37) || ($vdata5 > 21 && $vdata5 < 25)){
	 		$kondisi_suhu_luar = "Suhu diluar Mulai Terjadi Fluktuasi. ";
	 	}elseif(($vdata5 > 36 && $vdata5 < 42) || ($vdata5 > 17.99 && $vdata5 < 22)){
	 		$kondisi_suhu_luar = "Suhu diluar Mulai Terjadi Fluktuasi Tinggi. ";
	 	}elseif(($vdata5 > 32) || ($vdata5 < 18)){
	 		$kondisi_suhu_luar = "Suhu diluar Mulai Terjadi Fluktuasi Sangat Tinggi. ";
	 	}

	 	// ==== KELEMBABAN UDARA ==== //
	 	if(($vdata3 > 89 && $vdata3 < 94) || ($vdata3 > 29 && $vdata3 < 41)){
	 		$kondisi_kelembaban_u = "Kelembaban udara didalam Mulai Terjadi Fluktuasi. ";
	 	}elseif(($vdata3 > 93 && $vdata3 < 97) || ($vdata3 > 19 && $vdata3 < 30)){
	 		$kondisi_kelembaban_u = "Kelembaban udara didalam Mulai Terjadi Fluktuasi Tinggi. ";
	 	}elseif(($vdata3 > 96) || ($vdata3 < 20)){
	 		$kondisi_kelembaban_u = "Kelembaban udara didalam Mulai Terjadi Fluktuasi Sangat Tinggi. ";
	 	}

	 	// ==== KELEMBABAN TANAH ==== //
	 	if($vdata7 > 59 && $vdata7 < 81){
	 		$kondisi_kelembaban_t = "Kelembaban tanah didalam Mulai Terjadi Fluktuasi. ";
	 	}elseif($vdata7 > 59 && $vdata7 < 81){
	 		$kondisi_kelembaban_t = "Kelembaban tanah didalam Mulai Terjadi Fluktuasi Tinggi. ";
	 	}elseif($vdata7 > 80){
	 		$kondisi_kelembaban_t = "Kelembaban tanah didalam Mulai Terjadi Fluktuasi Sangat Tinggi. ";	
	 	}

	 	$data['name'] = 'Goa Rencang Kencono';
		$data['message'] = $kondisi_curah_hujan . ' dengan ' . $kondisi_gas . $kondisi_suhu_dalam . $kondisi_suhu_luar . $kondisi_tekan_u;
		$pusher->trigger('my-channel', 'my-event', $data);
		var_dump($data);
		

	     return TRUE;

	  } else {
	      return FALSE;
	}

	$db->close();
	}

?>

