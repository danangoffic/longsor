<nav id="nav" class="indigo accent-3" role="navigation">
    <div class="nav-wrapper">
        <a href="<?=url()?>" class="brand-logo center hide-on-large-only" style="font-size:18px;">GUA Indonesia - Yogyakarta</a>
        <!-- <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a> -->
        <ul class="left hide-on-med-and-down">
            <li <?=active('Gua Indonesia - Yogyakarta', $title)?>><a href="<?=url()?>">GUA Indonesia</a></li>
            <li <?=active('List Cave', $title)?>><a href="<?=url('list_cave.php')?>">List Cave</a></li>
            <li <?=active('Location', $title)?>><a href="<?=url('location.php')?>" >Location</a></li>
        </ul>
        <ul class="sidenav" id="nav-mobile">
            <li <?=active('Gua Indonesia - Yogyakarta', $title)?>><a href="<?=url()?>">GUA Indonesia</a></li>
            <li <?=active('List Cave', $title)?>><a href="<?=url('list_cave.php')?>">Cave List</a></li>
            <li <?=active('Location', $title)?>><a href="<?=url('location.php')?>" >Location</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>
