<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title><?=$title?></title>
    <meta name="token" content="<?=md5(uniqid(rand(), true));?>">
    <meta name="description" content="Gua Indonesia - Yogyakarta" />
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    <meta content="" name="author" />
    <meta property="og:site_name" content="Gua Indonesia - Yogyakarta">
    <meta property="og:description" content="Gua Indonesia - Yogyakarta" />
    <meta property="og:type" content="website"/>
    <meta name="theme-color" content="#3d5afe">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> -->
    <!-- <link href="<?=url()?>/library/materialize/icon-family.materialicons.css" rel="stylesheet" media="all"> -->
    <link href="<?=url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?=url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?=url('library/materialize/')?>/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.css" media="all"> -->
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?= url('library/plugins/morris/morris.css') ?>">
    <!-- <link rel="stylesheet" type="text/css" href="<?= url('library/plugins/datatables/dataTables.bootstrap.css') ?>"> -->
    <link rel="stylesheet" type="text/css" href="<?= url('library/weather-icons-master/css/weather-icons.css')?>">
    <link rel="stylesheet" type="text/css" href="<?= url('library/weather-icons-master/css/weather-icons-wind.css')?>">
        <!-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css" integrity="sha512-wcw6ts8Anuw10Mzh9Ytw4pylW8+NAD4ch3lqm9lzAsTxg0GFeJgoAtxuCLREZSC5lUXdVyo/7yfsqFjQ4S+aKw==" crossorigin=""> -->
        <!-- <script type="text/javascript" src="<?=url('library/plugins')?>/jquery-2.2.4.js"></script> -->

        <!-- <script type="text/javascript" src="<?= url('library/plugins/jQuery/jquery.flot.js') ?>"></script> -->
<!--        <script type="text/javascript" src="<?= url('library/plugins/flot/jquery.flot.resize.min.js') ?>"></script>
        <script type="text/javascript" src="<?= url('library/plugins/flot/jquery.flot.categories.min.js') ?>"></script>-->
    <style type="text/css">
        .collection .collection-item.active {
            background-color: #007fff;
            color: #eafaf9;
        }
        .cursor {
            cursor: pointer;
        }
        .pagination li.active {
            background-color: #007fff;
        }
        .input-field input[type="search"] ~ .mdi-navigation-close, .input-field input[type="search"] ~ .material-icons {
            color: whitesmoke;
        }
        .collection .collection-item {
            width: 33%;
            display: inline-table;
        }
        .chip .c {
            float: right;
            font-size: 16px;
            line-height: 32px;
            padding-left: 8px;
        }
        @media only screen and (max-width: 991px) {
            .collection .collection-item {
                width: 100%;
            }
        }

    </style>
</head>