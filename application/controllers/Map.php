<?php
/**
 * 
 */
class Map extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Map_model', 'model');
		$this->tt = 'maplocation';
	}

	public function index()
	{
		$data = array('tt'=>$this->tt);
		$this->load->view('maplocation', $data);
	}

	public function testing()
	{
		$data = array('data'=>$this->model->get_lokasi());
		$this->load->view('testing_maps', $data);
	}

	public function pop_up($lokasi)
	{
		$data_lokasi = $this->model->get_lokasi_by_id($lokasi);
		$data_hujan = $this->model->get_hujan_by_lokasi($lokasi);
		$data_getaran = $this->model->get_getaran_by_lokasi($lokasi);
		$data_kelembaban = $this->model->get_kelembaban_by_lokasi($lokasi);
		$data_suhu = $this->model->get_suhu_by_lokasi($lokasi);
		if ($data_hujan->curah_hujan == 0) {
    
		    //markering 1 (grey)
		    $color = "blue lighten-5";
		    $condt = "Normal";
		    ?> 
		    
		    <?php
		} else if ($data_hujan->curah_hujan >= 1 && $data_hujan->curah_hujan < 100) {
		    $color = "green lighten-3";
		    $condt = "Hujan Gerimis Ringan";
		    //markering 2 (green)
		} else if ($data_hujan->curah_hujan >= 101 && $data_hujan->curah_hujan <= 200) {
		    
		    //markering 3 (yellow)
		    $color = "lime lighten-3";
		    $condt = "Hujan Gerimis Sedang";
		} else if ($data_hujan->curah_hujan > 201 && $data_hujan->curah_hujan <= 300) {
		    
		    //markering 4 (orange)
		    $color = "orange lighten-3";
		    $condt = "Hujan Deras";
		} else if ($data_hujan->curah_hujan > 301 && $data_hujan->curah_hujan <= 500) {
		    
		    //markering 5 (red)
		    $color = "deep-orange darken-4";
		    $condt = "Hujan Sangat Deras";
		}

		if($data_getaran->frequensi==0){
			$colorg = "green accent-3";
			$condg = "Tidak Terjadi Getaran";
		}elseif($data_getaran->frequensi >= 1 && $data_getaran->frequensi <= 20){
			$colorg = "amber";
			$condg = "Terjadi Getaran Ringan";
		}elseif($data_getaran->frequensi >=21 && $data_getaran->frequensi <= 50){
			$colorg = "orange darken-3";
			$condg = "Getaran Meningkat Tinggi";
		}elseif($data_getaran->frequensi >= 51){
			$colorg = "deep-orange darken-4";
			$condg = "Terjadi Getaran Tanah Sangat Tinggi";
		}

		if($data_suhu->row(0)->suhu_tanah >= 29 && $data_suhu->row(0)->suhu_tanah <= 34){
			$colors1 = "green accent-3";
			$conds1 = "Suhu Sensor 1 Normal";
		}elseif($data_suhu->row(0)->suhu_tanah > 34 && $data_suhu->row(0)->suhu_tanah <= 36){
			$colors1 = "amber";
			$conds1 = "Suhu Sensor 1 Naik";
		}elseif($data_suhu->row(0)->suhu_tanah >= 22 && $data_suhu->row(0)->suhu_tanah < 29){
			$colors1 = "light-blue lighten-5";
			$conds1 = "Suhu Sensor 1 Turun";
		}elseif($data_suhu->row(0)->suhu_tanah > 36){
			$colors1 = "orange darken-3";
			$conds1 = "Suhu Sensor 1 sangat Tinggi";
		}elseif($data_suhu->row(0)->suhu_tanah < 22){
			$colors1 = "light-blue darken-3";
			$conds1 = "Suhu Sensor 1 Rendah";
		}

		if($data_suhu->row(1)->suhu_tanah >= 29 && $data_suhu->row(1)->suhu_tanah <= 34){
			$colors2 = "green accent-3";
			$conds2 = "Suhu Sensor 2 Normal";
		}elseif($data_suhu->row(1)->suhu_tanah > 34 && $data_suhu->row(1)->suhu_tanah <= 36){
			$colors2 = "amber";
			$conds2 = "Suhu Sensor 2 Naik";
		}elseif($data_suhu->row(1)->suhu_tanah >= 22 && $data_suhu->row(1)->suhu_tanah < 29){
			$colors2 = "light-blue lighten-5";
			$conds2 = "Suhu Sensor 2 Turun";
		}elseif($data_suhu->row(1)->suhu_tanah > 36){
			$colors2 = "orange darken-3";
			$conds2 = "Suhu Sensor 2 sangat Tinggi";
		}elseif($data_suhu->row(1)->suhu_tanah < 22){
			$colors2 = "light-blue darken-3";
			$conds2 = "Suhu Sensor 2 Rendah";
		}

		if($data_suhu->row(2)->suhu_tanah >= 29 && $data_suhu->row(2)->suhu_tanah <= 34){
			$colors3 = "green accent-3";
			$conds3 = "Suhu Sensor 3 Normal";
		}elseif($data_suhu->row(2)->suhu_tanah > 34 && $data_suhu->row(2)->suhu_tanah <= 36){
			$colors3 = "amber";
			$conds3 = "Suhu Sensor 3 Naik";
		}elseif($data_suhu->row(2)->suhu_tanah >= 22 && $data_suhu->row(2)->suhu_tanah < 29){
			$colors3 = "light-blue lighten-5";
			$conds3 = "Suhu Sensor 3 Turun";
		}elseif($data_suhu->row(2)->suhu_tanah > 36){
			$colors3 = "orange darken-3";
			$conds3 = "Suhu Sensor 3 sangat Tinggi";
		}elseif($data_suhu->row(2)->suhu_tanah < 22){
			$colors3 = "light-blue darken-3";
			$conds3 = "Suhu Sensor 3 Rendah";
		}
		$page_content = array(
			'lokasi' => $data_lokasi,
			'hujan' => $data_hujan,
			'getaran' => $data_getaran,
			'kelembaban' => $data_kelembaban,
			'suhu' => $data_suhu,
			'id_lokasi' => $lokasi,
		);
		return $this->load->view('popup', $page_content);
	}
}