<?php

/**
 * 
 */
class List_land extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('List_model', 'list');
		$this->tt = "list";
	}

	public function index()
	{
		$tt = $this->tt;
		$data = array(
			'tt'=>$tt,
			'list_lokasi'=>$this->list->get_lokasi(),
			'title'=>"Landslide Monitoring"
			);
		$this->load->view('list_view', $data);
	}


}