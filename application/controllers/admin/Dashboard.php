<?php
/**
 * 
 */
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('ADmin_model','admin_m');
	}

	public function index()
	{
		$this->load->view('admin/home', array('title'=>'Dashboard'));
	}
}