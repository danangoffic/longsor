<?php

/**
 * 
 */
class Detail extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Detail_model', 'model');
		$this->title = "detail";
	}

	public function index()
	{
		redirect(base_url('List_land'));
	}

	public function highlight($id,$lokasi)
	{
		$title = "Monitoring Longsor DETAIL LONGSOR ";
		$tt = "highlight";
		if(!isset($id)&&!isset($lokasi)){
			$data = array('title' => $title, 
							'tt' => $tt);
		}else{
			
			$year = substr($id, 0, 4);
			$day = substr($id, 4);
			$data_getaran = $this->model->get_data_getaran($day, $year, $lokasi);
			$data_hujan = $this->model->get_data_hujan($day, $year, $lokasi);
			$data_kelembaban = $this->model->get_data_kelembaban($day, $year, $lokasi);
			$data_suhu = $this->model->get_data_suhu($day, $year, $lokasi);
			$data_alat = $this->model->get_data_alat($lokasi);
			$data_lokasi = $this->model->get_data_lokasi($lokasi);
			$data = array('title' => $title, 
							'tt' => $tt, 
							'data_getaran' => $data_getaran, 
							'data_hujan' => $data_hujan,
							'data_kelembaban' => $data_kelembaban,
							'data_suhu' => $data_suhu,
							'data_alat' => $data_alat,
							'data_lokasi' => $data_lokasi,
							'id' => $id,
							'lokasi' => $lokasi,
							'highlight' => true);
		}
		
		$this->load->view('home_highlight', $data);

	}
	public function lokasi($id,$lokasi)
	{
		$year = substr($id, 0,4);
		$id = substr($id, 4);
		// $date = date_create($id);
		// $date = date_format($date, 'Y-m-d');
		// echo $date;
		// ========= ICON ========= //
		$icon_hujan = '';
		$icon_getaran = '';
		$icon_suhu1 = '';
		$icon_suhu2 = '';
		$icon_suhu3 = '';
		$icon_kelembaban1 = '';
		$icon_kelembaban2 = '';
		$icon_kelembaban3 = '';

		// ========= GRAPH ========= //
		$graph_hujan = '';
		$graph_getaran = '';
		$graph_suhu1 = '';
		$graph_suhu2 = '';
		$graph_suhu3 = '';
		$graph_kelembaban1 = '';
		$graph_kelembaban2 = '';
		$graph_kelembaban3 = '';
		// print($_GET['lokasi']);
	}

	public function graph($id_lokasi)
	{
		$lokasi = $this->model->get_data_lokasi($id_lokasi);
		$data = array('title' => "Detail Aktivitas Grafik " . $lokasi->lokasi, 
						'lokasi' => $lokasi,
						'tt' => "Detail Aktivitas Grafik " . $lokasi->lokasi, 
						'id_lokasi' => $id_lokasi,);
		$this->load->view('detail_graph', $data);
	}

	public function graph_data_getaran($id_lokasi = null)
	{
		if($id_lokasi!==null){
			header('Access-Control-Allow-Origin: *');
			header('Cache-Control: store');
			header('Pragma: cache');
			header('Content-type:application/json');
			$dari_data = $this->model->data_graph_getaran($id_lokasi);
			$lokasi = $this->model->get_data_lokasi($id_lokasi);
			$n = 0;
			foreach ($dari_data->result() as $row) {
				$data['data']['y'][] = $row->frequensi ;
				$data['data']['x'][] =  $row->jam;
				$n++;
			}
			$data['layout'] = array(
				'title' => 'Grafik Getaran Tanah ' . $lokasi->lokasi,
				'autosize' => true,
				'hovermode' => 'closest',
				'xaxis' => array(
					'range' => [-1.487084870848708, 25.487084870848708],
					'type' => 'category',
					'autorange' => true,
					'title' => 'Waktu (24 Jam Terakhir)'),
				'yaxis' => array(
					'range' => array(-1,1),
					'type' => 'linear',
					'autorange' => true,
					'title' => 'Frequensi Getaran (Hz)')
			);
			$data['frames'] = array();
			$data['data']['mode'] = 'lines+markers';
			$data['data']['type'] = 'scatter';
			$data['data']['name'] = 'Hz';
			print json_encode($data, JSON_NUMERIC_CHECK);
		}
	}

	public function graph_data_hujan($id_lokasi = null)
	{
		if($id_lokasi!==null){
			header('Access-Control-Allow-Origin: *');
			header('Cache-Control: store');
			header('Pragma: cache');
			header('Content-type:application/json');
			$dari_data = $this->model->data_graph_hujan($id_lokasi);
			$lokasi = $this->model->get_data_lokasi($id_lokasi);
			$n = 0;
			foreach ($dari_data->result() as $row) {
				$data['data']['y'][] = $row->curah_hujan ;
				$data['data']['x'][] =  $row->jam;
				$n++;
			}
			$data['layout'] = array(
				'title' => 'Grafik Curah Hujan ' . $lokasi->lokasi,
				'autosize' => true,
				'hovermode' => 'closest',
				'xaxis' => array(
					'range' => [-1.487084870848708, 25.487084870848708],
					'type' => 'category',
					'autorange' => true,
					'title' => 'Waktu (24 Jam Terakhir)'),
				'yaxis' => array(
					'range' => array(-1,1),
					'type' => 'linear',
					'autorange' => true,
					'title' => 'Ukuran Curah Hujan (mm)')
			);
			$data['frames'] = array();
			$data['data']['mode'] = 'lines+markers';
			$data['data']['type'] = 'line';
			$data['data']['name'] = 'mm';
			$data['data']['text'] = 'curah hujan';
			print json_encode($data, JSON_NUMERIC_CHECK);
		}
	}

	public function graph_data_suhu($id_lokasi = null)
	{
		if($id_lokasi!==null){
			header('Access-Control-Allow-Origin: *');
			header('Cache-Control: store');
			header('Pragma: cache');
			header('Content-type:application/json');
			$dari_alat = $this->model->get_alat_suhu($id_lokasi);
			$ss = 1;
			$data['data'] = array();
			foreach ($dari_alat->result() as $key) {
				$data_suhu = $this->model->get_graph_suhu($id_lokasi, $key->id_alat);
				foreach ($data_suhu->result() as $row_suhu) {
					$init['trace'.$ss]['y'][] = $row_suhu->suhu_tanah;
					$init['trace'.$ss]['x'][] = $row_suhu->jam;
					$init['trace'.$ss]['id_alat'] = $row_suhu->id_alat;
					$lokasi = $this->model->get_data_lokasi($id_lokasi);
					$data['frames'] = array();
					$init['trace'.$ss]['mode'] = 'lines+markers';
					$init['trace'.$ss]['type'] = 'line';
					$init['trace'.$ss]['name'] = $key->id_alat;
					$init['trace'.$ss]['text'] = 'Suhu Tanah';
				}
				array_push($data['data'], $init['trace'.$ss]);
				$ss++;
			}
			$data['layout'] = array(
						'title' => 'Grafik Suhu Tanah ' . $lokasi->lokasi,
						'autosize' => true,
						'hovermode' => 'closest',
						'xaxis' => array(
							'range' => [-1.487084870848708, 25.487084870848708],
							'type' => 'category',
							'autorange' => true,
							'title' => 'Waktu (24 Jam Terakhir)'),
						'yaxis' => array(
							'range' => array(-1,1),
							'type' => 'linear',
							'autorange' => true,
							'title' => 'Suhu Tanah (&deg;C)')
					);
			print json_encode($data, JSON_NUMERIC_CHECK);
		}
	}

	public function graph_data_kelembaban($id_lokasi = null)
	{
		if($id_lokasi!==null){
			header('Access-Control-Allow-Origin: *');
			header('Cache-Control: store');
			header('Pragma: cache');
			header('Content-type:application/json');
			$dari_alat = $this->model->get_alat_kelembaban($id_lokasi);
			$ss = 1;
			$data['data'] = array();
			foreach ($dari_alat->result() as $key) {
				$data_suhu = $this->model->get_graph_kelembaban($id_lokasi, $key->id_alat);
				foreach ($data_suhu->result() as $row_suhu) {
					$init['trace'.$ss]['y'][] = $row_suhu->kelembaban_tanah;
					$init['trace'.$ss]['x'][] = $row_suhu->jam;
					$init['trace'.$ss]['id_alat'] = $row_suhu->id_alat;
					$lokasi = $this->model->get_data_lokasi($id_lokasi);
					$data['frames'] = array();
					$init['trace'.$ss]['mode'] = 'lines+markers';
					$init['trace'.$ss]['type'] = 'line';
					$init['trace'.$ss]['name'] = $key->id_alat;
					$init['trace'.$ss]['text'] = 'Kelembaban Tanah';
				}
				array_push($data['data'], $init['trace'.$ss]);
				$ss++;
			}
			$data['layout'] = array(
						'title' => 'Grafik Kelembaban Tanah ' . $lokasi->lokasi,
						'autosize' => true,
						'hovermode' => 'closest',
						'xaxis' => array(
							'range' => [-1.487084870848708, 25.487084870848708],
							'type' => 'category',
							'autorange' => true,
							'title' => 'Waktu (24 Jam Terakhir)'),
						'yaxis' => array(
							'range' => array(-1,1),
							'type' => 'linear',
							'autorange' => true,
							'title' => 'Kadar Kelembaban Tanah (%)')
					);
			print json_encode($data, JSON_NUMERIC_CHECK);
		}
	}
}