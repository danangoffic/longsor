<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Home_model', 'modelhome');
	}

	public function index()
	{
		
		$title = "Monitoring Longsor";
		$tt = "home";
		$data = array('title' => $title, 'tt'=>$tt);
		$this->load->view($tt, $data);
	}

	public function get_all()
	{
		$this->modelhome->all_get();
		echo $this->db->last_query();
	}

	public function jsonhome()
	{
		$hari_ini='';
		header('Access-Control-Allow-Origin: *');
		header('Cache-Control: store');
		header('Pragma: cache');
		header('Content-type:application/json');
		$hexa ="";
		$kondisi_hujan = "";
		$levels = "";
		// $query_day = $this->db->query("SELECT data_hujan.tanggal, DAYOFYEAR(data_hujan.tanggal) AS harihujan,
		// 	YEAR(data_hujan.tanggal) AS tahun,
		// 	DAY(data_hujan.tanggal) AS harian,
		// 	data_lokasi.id_lokasi, data_lokasi.lokasi FROM data_hujan
		// 	JOIN data_alat ON data_alat.id_alat = data_hujan.id_alat
		// 	JOIN data_lokasi ON data_lokasi.id_lokasi = data_alat.id_lokasi
		// 	GROUP BY data_hujan.id_alat, harihujan, data_alat.id_lokasi
		// 	ORDER BY harihujan DESC");
			foreach ($this->modelhome->all_hujan()->result_array() as $var) {
				$query1 = $this->modelhome->get_hujan_by_lokasi_tgl($var['id_lokasi'], $var['tanggal']);
				// $query1 = $this->db->select('data_lokasi.lokasi, data_hujan.jam as jam_hujan, data_hujan.tanggal AS tanggalhujan, data_hujan.id_alat, data_hujan.curah_hujan, data_hujan.curah_hujan')
				// 						->from('data_hujan')
				// 						->join('data_alat', 'data_alat.id_alat = data_hujan.id_alat')
				// 						->join('data_lokasi', 'data_lokasi.id_lokasi = data_alat.id_lokasi')
				// 						->where('data_alat.id_lokasi', $var['id_lokasi'])
				// 						->where('data_hujan.tanggal', $var['tanggal'])
				// 						->order_by('data_hujan.nomor', 'DESC')
				// 						->limit(1)->get();
				// $data1 = $query1->row();
				// if($query1->num_rows()!==0):
					$query2 = $this->db->select('data_getaran.frequensi, data_getaran.id_alat, data_getaran.tanggal, data_getaran.jam, data_getaran.nomor')
											->from('data_getaran')
											->join('data_alat', 'data_alat.id_alat = data_getaran.id_alat')
											->join('data_lokasi', 'data_lokasi.id_lokasi = data_alat.id_lokasi')
											->where('data_lokasi.id_lokasi', $var['id_lokasi'])
											->where('data_getaran.tanggal', $var['tanggal'])
											->order_by('data_getaran.nomor', 'DESC')
											->limit(1)->get();
					// $data2 = $query2->row();
					
					$query3 = $this->db->select('data_kel_tanah.kelembaban_tanah, data_kel_tanah.id_alat, data_kel_tanah.tanggal, data_kel_tanah.jam, data_kel_tanah.nomor')
											->from('data_kel_tanah')
											->join('data_alat', 'data_alat.id_alat = data_kel_tanah.id_alat')
											->join('data_lokasi', 'data_lokasi.id_lokasi = data_alat.id_lokasi')
											->where('data_lokasi.id_lokasi', $var['id_lokasi'])
											->where('data_kel_tanah.tanggal', $var['tanggal'])
											->order_by('data_kel_tanah.nomor', 'DESC')
											->limit(3)->get();
					// $data3 = $query3->result();
					$query4= $this->db->select('data_su_tanah.suhu_tanah, data_su_tanah.id_alat, data_su_tanah.tanggal, data_su_tanah.jam, data_su_tanah.nomor')
											->from('data_su_tanah')
											->join('data_alat', 'data_alat.id_alat = data_su_tanah.id_alat')
											->join('data_lokasi', 'data_lokasi.id_lokasi = data_alat.id_lokasi')
											->where('data_lokasi.id_lokasi', $var['id_lokasi'])
											->where('data_su_tanah.tanggal', $var['tanggal'])
											->order_by('data_su_tanah.nomor', 'DESC')
											->limit(3)->get();
					// $data4 = $query4->result();
					$rows = $query1->row_array();
					$hari = date("D", strtotime($rows['tanggalhujan']));
	     			if($rows['curah_hujan'] < 21)
	     				{$hexa = '#01470a'; $kondisi_hujan = 'Curah Hujan Rendah'; $levels = 'Normal';}
	                elseif($rows['curah_hujan'] > 20 && $rows['curah_hujan'] < 51)
	                	{$hexa = '#389233'; $kondisi_hujan = 'Curah Hujan Rendah'; $levels = 'Normal';}
	                elseif($rows['curah_hujan'] > 50 && $rows['curah_hujan'] < 101)
	                	{$hexa = '#8bd68a'; $kondisi_hujan = 'Curah Hujan Rendah'; $levels = 'Normal';}
	                elseif($rows['curah_hujan'] > 100 && $rows['curah_hujan'] < 151)
	                	{$hexa = '#8bd68a'; $kondisi_hujan = 'Curah Hujan Menengah'; $levels = 'Normal';} 
	                elseif($rows['curah_hujan'] > 150 && $rows['curah_hujan'] < 201)
	                	{$hexa = '#ede200'; $kondisi_hujan = 'Curah Hujan Menengah'; $levels = 'Waspada';}
	                elseif($rows['curah_hujan'] > 200 && $rows['curah_hujan'] < 301)
	                	{$hexa = '#f0a900'; $kondisi_hujan = 'Curah Hujan Menengah'; $levels = 'Waspada';}
	                elseif($rows['curah_hujan'] > 300 && $rows['curah_hujan'] < 401)
	                	{$hexa = '#8f2900'; $kondisi_hujan = 'Curah Hujan Menengah'; $levels = 'Siaga';}
	                elseif($rows['curah_hujan'] > 400 && $rows['curah_hujan'] < 501)
	                	{$hexa = '#8f2900'; $kondisi_hujan = 'Curah Hujan Menengah'; $levels = 'Siaga';}
	                else
	                	{
	                	$hexa = '#350b00';
	                	$kondisi_hujan = 'Curah Hujan Tinggi';
	                	$levels = 'Awas';
	                	}
			        switch($hari){
			            case 'Sun':
			                $hari_ini = "Minggu";
			            break;
			     
			            case 'Mon':         
			                $hari_ini = "Senin";
			            break;
			     
			            case 'Tue':
			                $hari_ini = "Selasa";
			            break;
			     
			            case 'Wed':
			                $hari_ini = "Rabu";
			            break;
			     
			            case 'Thu':
			                $hari_ini = "Kamis";
			            break;
			     
			            case 'Fri':
			                $hari_ini = "Jumat";
			            break;
			     
			            case 'Sat':
			                $hari_ini = "Sabtu";
			            break;
			            
			            default:
			                $hari_ini = "salah";
			            break;
			        }
			        
			        $id = $var['tahun'] . $var['harihujan'];
			        if($query1->num_rows() > 0){
			        	$data1 = $query1->row();
			        	$tgl_hujan = $data1->tanggalhujan;
			        }else{
			        	$tgl_hujan = '';
			        }
			        if($query2->num_rows() > 0){
			        	$datarow2 = $query2->row();
			        	$jam_getaran_suhu_kelembaban = $datarow2->jam;
			        	$frequensi = $datarow2->frequensi;
			        	$datares2 = $query2->result();
			        	$datares3 = $query3->result();
			        	$datares4 = $query4->result();
			        	foreach ($datares3 as $key3) {
				        	$tanggal3[] = $key3->tanggal;
				        	$jam3[] = $key3->jam;
				        	$kelembaban_tanah[] = $key3->kelembaban_tanah;
				        	$id3[] = $key3->id_alat;
				        }

				        foreach ($datares4 as $key4) {
				        	$tanggal4[] = $key4->tanggal;
				        	$jam4[] = $key4->jam;
				        	$suhu_tanah[] = $key4->suhu_tanah;
				        	$id4 = $key4->id_alat;
				        }
			        }else{
			        	$jam_getaran_suhu_kelembaban = '';
			        	$kelembaban_tanah[0] = '';
			        	$suhu_tanah[0] = '';
			        	$frequensi = '';
			        }

			        if($query1->num_rows() == 0){
			        	$row[] = array('id_lokasi'=>$var['id_lokasi'],
										'nama_lokasi'=>$var['lokasi'],
										'curah_hujan'=>'0',
										'kondisi_hujan'=>'',
										'tanggal'=>$query2->row()->tanggal,
										'jam_suhu_kel_getaran'=>$jam_getaran_suhu_kelembaban,
										'hari'=>$hari_ini,
										'id'=>$id,
										'situasi_kel'=>'Memiliki kadar kelembaban tanah sebesar ' . $kelembaban_tanah[0] . '%',
										'situasi_suh'=>'Suhu dalam tanah adalah ' . $suhu_tanah[0] . '&deg;C',
										'situasi_get'=>'Getaran pada tanah adalah ' . $frequensi . 'Hz',
										'hexa'=>$hexa,
										'levels'=>$levels,
										'suhu_tanah'=>$suhu_tanah[0],
										'kelembaban_tanah'=>$kelembaban_tanah[0],
										'getaran'=>$frequensi,
										'nn'=>str_replace('-', '', $var['tanggal']) . '/' . $var['id_lokasi'] . $var['harihujan']
									);
			        }elseif($query1->num_rows!==0){
			        	$row[] = array('id_lokasi'=>$var['id_lokasi'],
										'nama_lokasi'=>$var['lokasi'],
										'tanggal'=>$data1->tanggalhujan,
										'jam_hujan'=>$data1->jam_hujan,
										'jam_suhu_kel_getaran'=>$jam_getaran_suhu_kelembaban,
										'hari'=>$hari_ini,
										'curah_hujan'=>$rows['curah_hujan'],
										'kondisi_hujan'=>$kondisi_hujan,
										'id'=>$id,
										'situasi_kel'=>'Memiliki kadar kelembaban tanah sebesar ' . $kelembaban_tanah[0] . '%',
										'situasi_suh'=>'Suhu dalam tanah adalah ' . $suhu_tanah[0] . '&deg;C',
										'situasi_get'=>'Getaran pada tanah adalah ' . $frequensi . 'Hz',
										'hexa'=>$hexa,
										'levels'=>$levels,
										'suhu_tanah'=>$suhu_tanah[0],
										'kelembaban_tanah'=>$kelembaban_tanah[0],
										'getaran'=>$frequensi,
										'nn'=>str_replace('-', '', $var['tanggal']) . '/' . $var['id_lokasi'] . $var['harihujan']
									);
			        }

					
				// endif;
			}
		echo json_encode($row);
		$this->db->close();
	}
}