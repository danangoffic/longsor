<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('login_modal'))
{
    function login_modal()
    {
        $aas = &get_instance();
	    return $aas->load->view('template/login_modal');
	    
    }   
}