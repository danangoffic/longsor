<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('bulan_func')) {
	function bulan_func($tanggal)
	{
	    $date = date_create($tanggal);
	    $bulan = array(
	                '01' => 'JANUARI',
	                '02' => 'FEBRUARI',
	                '03' => 'MARET',
	                '04' => 'APRIL',
	                '05' => 'MEI',
	                '06' => 'JUNI',
	                '07' => 'JULI',
	                '08' => 'AGUSTUS',
	                '09' => 'SEPTEMBER',
	                '10' => 'OKTOBER',
	                '11' => 'NOVEMBER',
	                '12' => 'DESEMBER',
	        );
	    return $bulan[date_format($date, 'm')];
	}
}
if (!function_exists('hari_func')) {
	function hari_func($hari){
	    $date = date_create($hari);
	    $hari = date_format($date, "D");
	 
	    switch($hari){
	        case 'Sun':
	            $hari_ini = "Minggu";
	        break;
	 
	        case 'Mon':         
	            $hari_ini = "Senin";
	        break;
	 
	        case 'Tue':
	            $hari_ini = "Selasa";
	        break;
	 
	        case 'Wed':
	            $hari_ini = "Rabu";
	        break;
	 
	        case 'Thu':
	            $hari_ini = "Kamis";
	        break;
	 
	        case 'Fri':
	            $hari_ini = "Jumat";
	        break;
	 
	        case 'Sat':
	            $hari_ini = "Sabtu";
	        break;
	        
	        default:
	            $hari_ini = "Tidak di ketahui";     
	        break;
	    }
	 
	    return $hari_ini;
	 
	}
}
