<?php
/**
 * 
 */
class Detail_model extends CI_Model
{
	public function get_data_getaran($id,$year,$lokasi)
	{
		$query = $this->db->select('dg.nomor, dg.frequensi, dg.tanggal, dg.jam, dl.lokasi, dl.id_lokasi')
							 ->from('data_alat da')
							 ->join('data_getaran dg', 'da.id_alat = dg.id_alat')
							 ->join('data_lokasi dl', 'da.id_lokasi = dl.id_lokasi')
							 ->where('YEAR(dg.tanggal)', $year)
							 ->where('DAYOFYEAR(dg.tanggal)', $id)
							 ->where('dl.id_lokasi', $lokasi)
							 ->order_by('dg.nomor', 'DESC')
							 ->order_by('dg.jam', 'DESC')
							 ->limit(1)
							 ->get();
		return $query->row();
	}

	public function get_data_hujan($id,$year,$lokasi)
	{
		$query = $this->db->select('dh.nomor, dh.curah_hujan, dh.tanggal, dh.jam, dh.id_alat, dl.lokasi, dl.id_lokasi')
							->from('data_alat da')
							->join('data_hujan dh', 'da.id_alat = dh.id_alat')
							->join('data_lokasi dl', 'da.id_lokasi = dl.id_lokasi')
							->where('YEAR(dh.tanggal)', $year)
							->where('DAYOFYEAR(dh.tanggal)', $id)
							->where('dl.id_lokasi', $lokasi)
							->order_by('dh.nomor', 'DESC')
							->order_by('dh.jam', 'DESC')
							->limit(1)
							->get();
		return $query->row();
	}

	public function get_data_kelembaban($id,$year,$lokasi)
	{
		$query = $this->db->select('dk.kelembaban_tanah, dk.tanggal, dk.jam, dk.nomor,  dl.lokasi, dl.id_lokasi, dk.id_alat')
							->from('data_alat da')
							->join('data_kel_tanah dk', 'da.id_alat = dk.id_alat')
							->join('data_lokasi dl', 'da.id_lokasi = dl.id_lokasi')
							->where('YEAR(dk.tanggal)', $year)
							->where('DAYOFYEAR(dk.tanggal)', $id)
							->where('dl.id_lokasi', $lokasi)
							->where_in('da.id_alat', "SELECT a.id_alat FROM data_alat a JOIN data_lokasi b ON a.id_lokasi = b.id_lokasi WHERE a.id_lokasi = ".$lokasi)
							->order_by('dk.nomor','DESC')
							->order_by('dk.jam', 'DESC')
							->limit(3)
							->get();
		return $query->result();							
	}

	public function get_data_suhu($id,$year,$lokasi)
	{
		$this->db->select('ds.nomor, ds.suhu_tanah, ds.id_alat, ds.tanggal, ds.jam, dl.lokasi');
		$this->db->from('data_alat da');
		$this->db->join('data_su_tanah ds', 'ds.id_alat = da.id_alat');
		$this->db->join('data_lokasi dl', 'dl.id_lokasi = da.id_lokasi');
		$this->db->where('YEAR(ds.tanggal) =', $year);
		$this->db->where('DAYOFYEAR(ds.tanggal) =', $id);
		$this->db->where('dl.id_lokasi', $lokasi);
		$this->db->order_by('ds.nomor', 'DESC');
		$this->db->order_by('ds.jam', 'DESC');
		$this->db->limit(3);
		return $this->db->get();
	}

	public function get_data_alat($lokasi)
	{
		$query = $this->db->select('id_alat, id_lokasi, nama_alat')
							->from('data_alat')
							->where('id_lokasi', $lokasi)
							->get();
		return $query;
	}

	public function get_data_lokasi($lokasi)
	{
		$query = $this->db->where('id_lokasi',$lokasi)->get('data_lokasi');
		return $query->row();
	}

	public function data_graph_getaran($id_lokasi)
	{
		$this->db->select('g.id_alat, g.tanggal, g.jam, g.frequensi');
		$this->db->from('data_getaran g');
		$this->db->join('data_alat a', 'g.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi =', $id_lokasi);
		$this->db->group_by('hour(g.jam)');
		$this->db->group_by('g.tanggal');
		$this->db->order_by('g.tanggal', 'DESC');
		$this->db->order_by('g.nomor', 'DESC');
		$this->db->limit(25);
		return $this->db->get();
	}

	public function data_graph_hujan($id_lokasi)
	{
		$this->db->select('h.id_alat, h.tanggal, h.jam, h.curah_hujan');
		$this->db->from('data_hujan h');
		$this->db->join('data_alat a', 'h.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi =', $id_lokasi);
		$this->db->group_by('hour(h.jam)');
		$this->db->group_by('h.tanggal');
		$this->db->order_by('h.tanggal', 'DESC');
		$this->db->order_by('h.nomor', 'DESC');
		$this->db->limit(25);
		return $this->db->get();
	}

	public function get_alat_suhu($id_lokasi)
	{
		$this->db->select('id_alat');
		$this->db->where('id_lokasi', $id_lokasi);
		$this->db->like('id_alat', 'S', 'after');
		return $this->db->get('data_alat');
	}

	public function get_alat_kelembaban($id_lokasi)
	{
		$this->db->select('id_alat');
		$this->db->where('id_lokasi', $id_lokasi);
		$this->db->like('id_alat', 'K', 'after');
		return $this->db->get('data_alat');
	}

	public function get_graph_suhu($id_lokasi, $id_alat)
	{
		$this->db->select('s.id_alat, s.tanggal, s.jam, s.suhu_tanah');
		$this->db->from('data_su_tanah s');
		$this->db->join('data_alat a', 's.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi =', $id_lokasi);
		$this->db->where('s.id_alat', $id_alat);
		$this->db->group_by('hour(s.jam)');
		$this->db->group_by('s.tanggal');
		$this->db->order_by('s.tanggal', 'DESC');
		$this->db->order_by('s.nomor', 'DESC');
		$this->db->limit(24);
		return $this->db->get();
	}

	public function get_graph_kelembaban($id_lokasi, $id_alat)
	{
		$this->db->select('k.id_alat, k.tanggal, k.jam, k.kelembaban_tanah');
		$this->db->from('data_kel_tanah k');
		$this->db->join('data_alat a', 'k.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi =', $id_lokasi);
		$this->db->where('k.id_alat', $id_alat);
		$this->db->group_by('hour(k.jam)');
		$this->db->group_by('k.tanggal');
		$this->db->order_by('k.tanggal', 'DESC');
		$this->db->order_by('k.nomor', 'DESC');
		$this->db->limit(24);
		return $this->db->get();
	}
}