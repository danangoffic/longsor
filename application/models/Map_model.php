<?php
/**
 * 
 */
class Map_Model extends CI_Model
{
	
	public function get_lokasi()
	{
		return $this->db->get('data_lokasi');
	}

	public function get_lokasi_by_id($id_lokasi)
	{
		$query = $this->db->select('*')
					->from('data_lokasi')
					->where('id_lokasi',$id_lokasi)
					->get();
		return $query->row();
	}

	public function get_hujan_by_lokasi($id_lokasi)
	{
		$this->db->select('*')
					->from('data_alat, data_hujan')
					->where('data_alat.id_lokasi', $id_lokasi)
					->where('data_alat.id_alat = data_hujan.id_alat')
					->order_by('nomor', 'DESC')
					->limit(1);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_getaran_by_lokasi($id_lokasi)
	{
		$this->db->select('g.frequensi AS frequensi, g.id_alat, g.tanggal, g.jam, g.nomor')
				->from('data_getaran g')
				->join('data_alat a', 'a.id_alat = g.id_alat')
				->where('a.id_lokasi', $id_lokasi)
				->order_by('nomor', 'DESC')
				->limit(1);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_kelembaban_by_lokasi($id_lokasi)
	{
		$this->db->select('k.kelembaban_tanah, k.id_alat, k.tanggal, k.jam, k.nomor')
					->from('data_kel_tanah k')
					->join('data_alat a', 'a.id_alat = k.id_alat')
					->where('a.id_lokasi', $id_lokasi)
					->order_by('nomor', 'DESC')
					->limit(3);
		return $this->db->get();
	}

	public function get_suhu_by_lokasi($id_lokasi)
	{
		$this->db->select('s.suhu_tanah, s.id_alat, s.tanggal, s.jam, s.nomor')
					->from('data_su_tanah s')
					->join('data_alat a', 'a.id_alat = s.id_alat')
					->where('a.id_lokasi', $id_lokasi)
					->order_by('nomor', 'DESC')
					->limit(3);
		return $this->db->get();
	}
}