<?php
/**
 * 
 */
class Home_model extends CI_Model
{
	
	public function json_data()
	{
		
		$query = $this->db->query("SELECT data_hujan.tanggal, data_hujan.jam,DAYOFYEAR(data_hujan.tanggal) AS hari , data_hujan.id_alat, data_hujan.curah_hujan, 
                                IF(data_hujan.curah_hujan < 101, 'Curah Hujan Rendah', 
                                IF(data_hujan.curah_hujan < 501, 'Curah Hujan Menengah', 'Curah Hujan Tinggi')) AS kondisi_hujan, 
                                IF(data_hujan.curah_hujan < 21, '#01470a', 
                                IF(data_hujan.curah_hujan < 51, '#389233', 
                                IF(data_hujan.curah_hujan < 101, '#8bd68a',
                                IF(data_hujan.curah_hujan < 151, '#e2fe64', 
                                IF(data_hujan.curah_hujan < 201, '#ede200', 
                                IF(data_hujan.curah_hujan < 301, '#f0a900', 
                                IF(data_hujan.curah_hujan < 401, '#8f2900', 
                                IF(data_hujan.curah_hujan < 501, '#8f2900', '#350b00')))))))) AS hexa,
                                IF(data_hujan.curah_hujan < 101, 'Normal', 
                                IF(data_hujan.curah_hujan < 301, 'Waspada', 
                                IF(data_hujan.curah_hujan < 501, 'Siaga', 'Awas'))) AS levels,
                                data_lokasi.id_lokasi, data_lokasi.lokasi
                                FROM data_hujan
                                JOIN data_alat ON data_hujan.id_alat = data_alat.id_alat
                                JOIN data_lokasi ON data_alat.id_lokasi = data_lokasi.id_lokasi
                                GROUP BY hari
                                ORDER BY hari DESC");
		return $query->result_array();
	}

        public function get_all_single($lokasi, $hari, $harihujan)
        {
                # code...
        }

        public function all_hujan()
        {
                $query = $this->db->query('SELECT g.tanggal, g.jam, a.id_alat, a.id_lokasi, l.lokasi, g.frequensi, DAYOFYEAR(g.tanggal) AS harihujan, YEAR(g.tanggal) AS tahun, DAY(g.tanggal) AS harian 
                        FROM data_getaran g 
                        JOIN data_hujan h
                        ON h.tanggal = g.tanggal
                        JOIN data_alat a 
                        ON a.id_alat = g.id_alat 
                        JOIN data_lokasi l 
                        ON l.id_lokasi = a.id_lokasi 
                        GROUP BY g.tanggal 
                        ORDER BY g.tanggal DESC, g.nomor DESC');
                return $query;
        }

        public function all_get()
        {
                return $this->db->query('SELECT g.tanggal, g.jam, a.id_alat, a.id_lokasi, l.lokasi, g.frequensi, DAYOFYEAR(g.tanggal) AS harihujan, YEAR(g.tanggal) AS tahun,
                                DAY(g.tanggal) AS harian
                                        FROM data_getaran g
                                        JOIN data_alat a
                                        ON a.id_alat = g.id_alat
                                        JOIN data_lokasi l
                                        ON l.id_lokasi = a.id_lokasi
                                        GROUP BY g.tanggal
                                        ORDER BY g.tanggal DESC, g.nomor DESC');
        }


        public function get_hujan_by_lokasi_tgl($lokasi, $tanggal)
        {
                $this->db->select('data_lokasi.lokasi, data_hujan.jam AS jam_hujan, data_hujan.tanggal AS tanggalhujan, data_hujan.id_alat, data_hujan.curah_hujan, data_hujan.curah_hujan');
                $this->db->from('data_hujan');
                $this->db->join('data_getaran', 'data_getaran.tanggal = data_hujan.tanggal');
                $this->db->join('data_alat', 'data_alat.id_alat = data_hujan.id_alat');
                $this->db->join('data_lokasi', 'data_lokasi.id_lokasi = data_alat.id_lokasi');
                $this->db->where('data_alat.id_lokasi', $lokasi);
                $this->db->where('data_hujan.tanggal', $tanggal);
                $this->db->group_by('data_hujan.id_alat');
                $this->db->group_by('data_hujan.tanggal');
                $this->db->group_by('data_alat.id_lokasi');
                $this->db->order_by('data_hujan.nomor', 'DESC');
                $this->db->limit(1);
                return $this->db->get();
        }

        public function get_getaran_by_lokasi_day($lokasi, $day)
        {
                $this->db->select('data_getaran.frequensi, data_getaran.id_alat, data_getaran.tanggal, data_getaran.jam, data_getaran.nomor');
                $this->db->from('data_getaran');
                $this->db->join('data_alat', 'data_alat.id_alat = data_getaran.id_alat');
                $this->db->join('data_lokasi', 'data_lokasi.id_lokasi = data_alat.id_lokasi');
                $this->db->where('data_lokasi.id_lokasi', $lokasi);
                $this->db->where('DAYOFYEAR(data_getaran.tanggal)', $day);
                $this->db->order_by('data_getaran.nomor', 'DESC');
                $this->db->limit(1);
                return $this->db->get();
        }

        public function save_hujan($data)
        {
                return $this->db->insert('data_hujan', $data);
        }

        public function save_getaran($data)
        {
                return $this->db->insert('data_getaran', $data);
        }

        public function save_kelembaban($data)
        {
                return $this->db->insert('data_kel_tanah', $data);
        }

        public function save_suhu($data)
        {
                return $this->db->insert('data_su_tanah', $data);
        }
}