<?php
/**
 * 
 */
class List_model extends CI_Model
{
	public function get_lokasi()
	{
		// $this->db->select('*');
		// $this->db->from();
		$query = $this->db->get('data_lokasi');
		return $query->result();
	}

	public function get_hujan1($id_lokasi)
	{
		$this->db->select('curah_hujan, data_alat.id_alat, tanggal, jam, DAYOFYEAR(data_hujan.tanggal) AS hari, MONTH(data_hujan.tanggal) AS bulan, YEAR(data_hujan.tanggal) AS tahun');
		$this->db->from('data_hujan, data_lokasi');
		$this->db->join('data_alat', 'data_hujan.id_alat = data_alat.id_alat AND data_alat.id_lokasi = data_lokasi.id_lokasi');
		$this->db->where('data_lokasi.id_lokasi', $id_lokasi);
		$this->db->order_by('nomor', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query;
	}

	public function get_getaran1($id_lokasi)
	{
		$this->db->select('frequensi, data_alat.id_alat, tanggal, jam, DAYOFYEAR(tanggal) AS hari, MONTH(tanggal) AS bulan, YEAR(tanggal) AS tahun');
		$this->db->from('data_getaran, data_lokasi');
		$this->db->join('data_alat', 'data_getaran.id_alat = data_alat.id_alat AND data_alat.id_lokasi = data_lokasi.id_lokasi');
		$this->db->where('data_lokasi.id_lokasi', $id_lokasi);
		$this->db->order_by('nomor', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query;
	}

	public function get_kelembaban1($id_lokasi)
	{
		$this->db->select('kelembaban_tanah, data_alat.id_alat, tanggal, jam, nomor, DAYOFYEAR(tanggal) AS hari, MONTH(tanggal) AS bulan, YEAR(tanggal) AS tahun');
		$this->db->from('data_kel_tanah, data_lokasi');
		$this->db->join('data_alat', 'data_kel_tanah.id_alat = data_alat.id_alat AND data_alat.id_lokasi = data_lokasi.id_lokasi');
		$this->db->where('data_lokasi.id_lokasi', $id_lokasi);
		$this->db->order_by('nomor', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query;
	}

	public function get_suhu1($id_lokasi)
	{
		$this->db->select('suhu_tanah, data_alat.id_alat, tanggal, jam, nomor, DAYOFYEAR(tanggal) AS hari, MONTH(tanggal) AS bulan, YEAR(tanggal) AS tahun');
		$this->db->from('data_su_tanah, data_lokasi');
		$this->db->join('data_alat', 'data_su_tanah.id_alat = data_alat.id_alat AND data_alat.id_lokasi = data_lokasi.id_lokasi');
		$this->db->where('data_lokasi.id_lokasi', $id_lokasi);
		$this->db->order_by('nomor', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query;
	}
}