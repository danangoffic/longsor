<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('template/head'); ?>
<style>
            @page {
                size: A4;
                margin: 0;
            }

            @media print {
                body * {
                    visibility: hidden;
                }
                #section-to-print, #section-to-print * {
                    visibility: visible;
                }
                #section-to-print {
                    display: block;
                    width: 21cm;
                    height: 29.5cm;
                    margin: 0;
                    position: absolute;
                    left: 0;
                    top: 0;
                }
                .card-action, .card-action * {
                    visibility: hidden !important;
                }                
            }

        </style>
</head>
<body>
<?php $this->load->view('template/nav'); ?>

	<div class="section no-pad-bot" id="index-banner">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col s12 m6 l6 xl6" style="margin-bottom: 10px !important; margin-right: none; margin-left: none;">
	                <div id="graph_hujan" style="width: 100%; height: 450px !important;">
	                </div>
	            </div>

	            <div class="col s12 m6 l6 xl6" style="margin-bottom: 10px !important; margin-right: none; margin-left: none;">
	            	<div id="graph_getaran" style="width: 100%; height: 450px !important;"></div>
	            </div>

	            <div class="col s12 m6 l6 xl6" style="margin-bottom: 10px !important; margin-right: none; margin-left: none;">
	            	<div id="graph_suhu" style="width: 100%; height: 450px !important;"></div>
	            </div>

	            <div class="col s12 m6 l6 xl6" style="margin-bottom: 10px !important; margin-right: none; margin-left: none;">
	            	<div id="graph_kelembaban" style="width: 100%; height: 450px !important;"></div>
	            </div>
	        </div>
	    </div>
	</div>
	<?php login_modal();?>

<?php $this->load->view('template/jsfoot'); ?>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var plotGetaran = document.getElementById('graph_getaran');
		$.getJSON("<?=base_url('detail/graph_data_getaran/').$id_lokasi;?>", function(res) {
			var layout = res.layout;
			var data1 = [res.data];
			Plotly.newPlot(plotGetaran, data1, layout);
		});

		var plotHujan = document.getElementById('graph_hujan');
		$.getJSON("<?=base_url('detail/graph_data_hujan/').$id_lokasi;?>", function(res2) {
			var layout2 = res2.layout;
			var data2 = [res2.data];
			Plotly.newPlot(plotHujan, data2, layout2);
		});

		var plotsuhu = document.getElementById('graph_suhu');
		$.getJSON("<?=base_url('detail/graph_data_suhu/').$id_lokasi;?>", function(res3) {
			var layout3 = res3.layout;
			var data3 = res3.data;
			Plotly.newPlot(plotsuhu, data3, layout3);
		});

		var plotkelembaban = document.getElementById('graph_kelembaban');
		$.getJSON("<?=base_url('detail/graph_data_kelembaban/').$id_lokasi;?>", function(res4) {
			var layout4 = res4.layout;
			var data4 = res4.data;
			Plotly.newPlot(plotkelembaban, data4, layout4);
		});
		// $.ajax({
		// 	url: "",
		// 	type: "GET",
		// 	dataType: "JSON",
		// 	success: function(data){
				
		// 		// console.log( Plotly.BUILD );
		// 	}
		// });
	});
</script>
</body>
</html>