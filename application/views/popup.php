<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>
<body>
	<div class="col s12 m12 l12 xl12" >
		<ul class="collection with-header" style="margin-top: 0 !important;">
			<li class="collection-header blue lighten-2"><strong><?=$lokasi->lokasi;?></strong></li>
			<li class="collection-header green accent-1"><strong>Location</strong></li>
	     	<li class="collection-item">
	     		<strong class="blue-text">Longitude:</strong> <?=$lokasi->lon;?><br>
	     		<strong class="blue-text">Latitude:</strong> <?=$lokasi->lat;?>
	     	</li>
	     	<li class="collection-header green accent-1"><strong>Waktu Pengamatan Terakhir</strong></li>
	     	<li class="collection-item">
	     		<strong class="blue-text">Tanggal: </strong><?=$hujan->tanggal;?><br>
	     		<strong class="blue-text">Jam: </strong><?=$hujan->jam . " WIB";?>
	     	</li>
	     	<li class="collection-header green accent-1"><strong>Kondisi</strong></li>
	     	<li class="collection-item">
	     		<strong class="blue-text">Suhu:</strong>
	     		<?=$suhu->row(0)->suhu_tanah."&deg;C";?> | <?=$suhu->row(1)->suhu_tanah."&deg;C";?> | <?=$suhu->row(2)->suhu_tanah."&deg;C";?>
	     	</li>
	     	<li class="collection-item">
	     		<strong class="blue-text">Kelembaban:</strong>
	     		<?=$kelembaban->row(0)->kelembaban_tanah."%"?> | <?=$kelembaban->row(1)->kelembaban_tanah."%";?> | <?=$kelembaban->row(2)->kelembaban_tanah."%";?>
	     	</li>
	     	<li class="collection-item">
	     		<strong class="blue-text">Getaran:</strong>
	     		<?=$getaran->frequensi;?>
	     	</li>
	     	<li class="collection-item">
	     		<strong class="blue-text">Curah Hujan:</strong>
	     		<?=$hujan->curah_hujan." mm";?>
	     	</li>
	     	<li class="collection-header green accent-1"><strong>Last Issued ID</strong></li>
	     	<li class="collection-item">
	     		<span class="orange-text"><?=str_replace('-', '', $hujan->tanggal);?>/<?=$id_lokasi;?></span>
	     	</li>
	    </ul>
    </div>
</body>
</html>