<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('template/head'); ?>
</head>
<body>
<?php $this->load->view('template/nav'); 

?>
    <div class="section no-pad-bot" id="index-banner">
	    <div class="container" style="margin-top: 5px;z-index: -2;" ng-app="appl" ng-controller="detCtrl">
            <div class="row" >
                <div id="sec2" class="col s12 m12">
                    <div class="card" style="">

                        <div class="card-content" id="section-to-print">

                            <span id="header" class="card-title"><b><?=$data_lokasi->lokasi;?> <?=$id?>&nbsp;/&nbsp;<?=$lokasi?></b></span>

                            <table class="" style="">
                                <tbody >
                                    <tr>
                                        <td>(1)</td>
                                        <td colspan="3" style="word-wrap:break-word;">
                                            <b>Residential Area</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>(2)</td>
                                        <td><b>Id Issued</b></td>
                                        <td><b>:</b></td>
                                        <td id="issued"><?=$id?>&nbsp;/&nbsp;<?=$lokasi?></td>
                                    </tr>
                                    <tr>
                                        <td>(3)</td>
                                        <td><b>Area</b></td>
                                        <td><b>:</b></td>
                                        <td id="lokasi"><?=$data_lokasi->lokasi;?> (<?=$lokasi?>)</td>
                                    </tr>
                                    <tr>
                                        <td>(4)</td>
                                        <td><b>Current Color Code</b></td>
                                        <td><b>:</b></td>
                                        <?php
                                        $n = 0;
                                        if(!empty($data_kelembaban)){
                                            foreach ($data_kelembaban as $row_k) {
                                                $n++;
                                                if($n==1){
                                                    $kelembaban1 = $row_k->kelembaban_tanah;
                                                }elseif($n==2){
                                                    $kelembaban2 = $row_k->kelembaban_tanah;
                                                }elseif ($n==3) {
                                                    $kelembaban3 = $row_k->kelembaban_tanah;
                                                }
                                            }
                                            $n=0;
                                        }
                                        if(($data_suhu->num_rows() !== 0)):
                                            foreach ($data_suhu->result() as $row_s) {
                                                $n++;
                                                if($n==1){
                                                    $suhu1 = $row_s->suhu_tanah;
                                                }elseif($n==2){
                                                    $suhu2 = $row_s->suhu_tanah;
                                                }elseif($n==3){
                                                    $suhu3 = $row_s->suhu_tanah;
                                                }
                                            }
                                            $n=0;
                                        endif;
                                        if(!empty($data_getaran->frequensi)==0 && !empty($data_hujan->curah_hujan) < 20 ){

                                        }
                                        ?>
                                        <td><b id="ccc"></b></td>
                                    </tr>
                                    <tr>
                                        <td>(5)</td>
                                        <td><b>Source</b></td>
                                        <td><b>:</b></td>
                                        <td id="source">
                                        <ul>
                                        <?php 
                                        $total_alat = $data_alat->num_rows();
                                        $num_alat = 1;
                                        foreach ($data_alat->result() as $alat) {
                                            echo "<li>".$alat->nama_alat . " (".$alat->id_alat.")</li>";
                                            $num_alat++;
                                        } ?>
                                        </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>(7)</td>
                                        <td><b>Land Location (Coordinate)</b></td>
                                        <td><b>:</b></td>
                                        <td id="loc">
                                            <?php
                                            if(!empty($data_lokasi->lat)){
                                                echo $data_lokasi->lat;
                                            }if(!empty($data_lokasi->lon)){
                                                echo ", " . $data_lokasi->lon;
                                            }if(empty($data_lokasi->lat)){
                                                echo "<span class='red-text'>Latitude Kosong</span>";
                                            }if(empty($data_lokasi->lon)){
                                                echo ", <span class='red-text'>Longitude Kosong</span>";
                                            }
                                            ?></td>
                                    </tr>
                                    <?php if($data_suhu->num_rows() > 0): ?>
                                    <tr>
                                        <td>(11)</td>
                                        <td><b>Summary of Exterior Degree</b></td>
                                        <td><b>:</b></td>
                                        <td id="suhu">
                                            <?= "<strong>".$data_suhu->row(0)->suhu_tanah."</strong><br>";?>
                                            <?= "<strong>".$data_suhu->row(1)->suhu_tanah."</strong><br>";?>
                                            <?= "<strong>".$data_suhu->row(2)->suhu_tanah."</strong><br>";?>
                                        </td>
                                    </tr>
                                    <?php endif; ?>

                                    <?php if(!empty($data_kelembaban)): ?>
                                    <tr>
                                        <td>(13)</td>
                                        <td><b>Summary of Soil Moisture</b></td>
                                        <td><b>:</b></td>
                                        <td id="kelembabant">
                                            <p class="text-bold"><?=(isset($kelembaban1) ? $kelembaban1 : "");?></p>
                                            <p class="text-bold"><?=(isset($kelembaban2) ? $kelembaban2 : "");?></p>
                                            <p class="text-bold"><?=(isset($kelembaban3) ? $kelembaban3 : "");?></p>
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <span class="card-title">
                                <b><?=$data_lokasi->lokasi;?> <?=$id?>&nbsp;/&nbsp;<?=$lokasi?></b>
                            </span>
                        </div>
                        <div class="card-action">
                            <button id="printing" class="btn waves-effect waves-light" type="button" style="background-color:#007fff;">Print</button>
                            <a href='#' id="cmd" class="btn waves-effect waves-light">PDF</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

	</div>
	<?php login_modal();?>

<?php $this->load->view('template/jsfoot'); ?>

</body>
</html>