<!-- jQuery 3 -->
<script src="<?=base_url('assets2/')?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url('assets2/')?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Material Design -->
<script src="<?=base_url('assets2/')?>/dist/js/material.min.js"></script>
<script src="<?=base_url('assets2/')?>/dist/js/ripples.min.js"></script>
<script>
    $.material.init();
</script>
<!-- SlimScroll -->
<script src="<?=base_url('assets2/')?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url('assets2/')?>/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('assets2/')?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes 
<script src="../../dist/js/demo.js"></script>-->
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>