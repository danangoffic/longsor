<!DOCTYPE html>
<html>

<?php $this->load->view('admin/templates/head'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('admin/templates/header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('admin/templates/sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title; ?>
        <!--small>it all starts here</small-->
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="<?=base_url('control');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Title</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div id="line-chart"></div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            Footer
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('admin/templates/footer'); ?>
    </div>
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

  <?php $this->load->view('admin/templates/scriptjs'); ?>
  <script type="text/javascript" src="<?=base_url('assets2/plugins/morris/raphael-min.js');?>"></script>
  <script type="text/javascript" src="<?=base_url('assets2/plugins/morris/morris.min.js');?>"></script>
  <script type="text/javascript">
    var area_line = Morris.Line({
      element: 'line-chart', 
      behaveLikeLine: true,
      parseTime : false, 
      data: [{ y: '2018-09-15 00:00:01', a: 100},
        { y: '2018-09-15 00:01:00', a: 75},
        { y: '2018-09-15 00:02:05', a: 50},
        { y: '2018-09-15 00:03:17', a: 75},
        { y: '2018-09-15 00:04:01', a: 50},
        { y: '2018-09-15 00:05:01', a: 75},
        { y: '2018-09-15 00:06:01', a: 100}],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Item 1'],
      lineColors: ['#3c8dbc'],
      hideHover: 'auto',
      pointFillColors: ['#707f9b'],
      // pointStrokeColors: ['#ffaaab'],
      
      redraw: true,
    })
  </script>
</body>
</html>
