<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('template/head'); ?>

<style type="text/css">
	.collection .collection-item.active {
            background-color: #007fff;
            color: #eafaf9;
        }
    .cursor {
        cursor: pointer;
    }
    .pagination li.active {
        background-color: #007fff;
    }
    .input-field input[type="search"] ~ .mdi-navigation-close, .input-field input[type="search"] ~ .material-icons {
        color: white;
    }
    .collection .collection-item {
        width: 33.333%;
        display: inline-table;
    }
    .chip .c {
        float: right;
        font-size: 16px;
        line-height: 32px;
        padding-left: 8px;
    }
    @media only screen and (max-width: 991px) {
        .collection .collection-item {
            width: 100%;
        }
    }

</style>
</head>
<body>
<?php $this->load->view('template/nav'); ?>

	<div class="section no-pad-bot" id="index-banner">
	    <div class="container">
	        <div class="row">
	            <div class="col s12 m12">
	                <div class="row">
	                    <div class="col s12">
	                       <div class="card">
	                            <div class="card-content">
	                                <div class="card-title judul"></div>
	                                <p class="isi"></p>
	                            </div>
	                            <div class="card-action">
	                                <a id="details" class="waves-effect waves-light btn" style="background-color: #007fff;" target="_blank">details</a>
	                                <div class="chip right red nn" style="color:white"></div>
	                            </div>
	                        </div> 
	                    </div>
	                </div>
	                
	            </div>
	        </div>
	        <div class="row">
	            <div class="col s12 m12">
	                <div id="data-list">

	                    <nav class="indigo accent-3">
	                        <div class="nav-wrapper">
	                          
	                            <div class="input-field">
	                              <input id="search" type="search" class="search" required placeholder="Cari Nama Tempat Rawan Longsor [ex: Karang Tengah], Tanggal [ex: HARI || TANGGAL || TAHUN || NAMA_BULAN], Level..">
	                              <label class="label-icon" for="search"><i class="material-icons">search</i></label>
	                            </div>
	                          
	                        </div>
	                    </nav>
	                    <ul class="list collection left-align" id="list">
	                        <li class="collection-item left-align" style="z-index: 10;">

	                        </li>
	                    </ul>
	                    <ul class="pagination pager" id="pager" >

	                    </ul>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<?php login_modal();?>

<?php $this->load->view('template/jsfoot'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		listdata();
	});
	var lists = '';
        var stats = '';
        var color = '';
        var col = '';
        var $home = '<?=base_url()?>';
        var flag = 0;
        var $level = "";
        // end subscribe
        function page() {
            var monitorlist = new List('data-list', {
                    valueNames: ['title', 'tgl', 'alamat', 'color'],
                    page: 9,
                    pagination: true,
                    plugins: [ListPagination({})]
                });
        }
        var option = {
                    valueNames: ['title', 'tgl', 'alamat', 'color'],
                    page: 9,
                    pagination: true,
                    plugins: [ListPagination({})]
                }

        var li = new List('data-list', option);
        
        function listdata() {
        	list='';
        	$.post($home+'home/jsonhome', function(result){
				$.each(result, function(i, field){
					if(i==0){
		                    var isi = '<b>Situasi Saat ini pada '+  field.nama_lokasi.replace(',', '') +' adalah ' + field.levels + '</b> yang memiliki curah hujan ' + field.curah_hujan + 'mm. ' + 
		                                field.situasi_suh + '. ' + field.situasi_kel + '. ' + field.situasi_get + '. ';
		                    $(".isi").html(isi);
		                    $(".judul").html(field.name);
		                    $("#details").attr('value', field.id);
		                    $("#details").attr('href', $home+"detail/highlight/"+field.id+"/"+field.id_lokasi);
		                    $(".nn").html(field.nn);
		                }
		                list = list+'<li class="collection-item avatar" style="z-index: 10;">' +
		                    '<div id="' +field.id+ '" class="cursor" lokasi="'+field.id_lokasi+'">' +
		                    '<i class="material-icons circle tooltipped" style="background-color: '+field.hexa+'" data-position="bottom" data-delay="50" data-tooltip="Kondisinya adalah ' + field.levels+ '">directions_walk</i>' +
		                    '<span class="title" style="border-bottom:1px solid #DDD">' + field.nama_lokasi+ '</br>('+field.levels+')</span>' +
		                    '<p class="tgl" id="tgl">'+field.tanggal+'<br>' +
		                    '<small class="hide tgl3" id="tgl3">'+field.tahun+'<br></small>' +
		                    '<small class="hide tgl4" id="tgl4">'+field.hari+'<br></small>' +
		                    '<small class="hide tgl5" id="tgl5">'+field.bulan+'<br></small>' +
		                    '</p>' +
		                    '</div>' +
		                '</li>';
				});
				$("#list").html(list);
                page();
                $('.tooltipped').tooltip({delay: 50});
			});
        }
        // setInterval(listdata, 30000);
	$(document).on('click', 'ul.list li div.cursor', function () {
        var $id = $(this).attr('id');
        var lokasi = $(this).attr('lokasi');
        window.open('<?= base_url('detail/highlight/') ?>' + $id + '/'+lokasi, '_blank');
//        alert();
    });
</script>

</body>
</html>