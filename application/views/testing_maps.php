<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Testing Maps</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

<!--        <link rel="stylesheet" src="<?=base_url()?>library/plugins/leaflet/leaflet.css"/>
        <script src="<?=base_url()?>library/plugins/leaflet/leaflet.js" ></script>-->

         <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
          integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
          crossorigin=""></script> 
        <!-- <script src="<?=base_url()?>data_maps/countries.geojson"></script> -->
        <style>
            #map{
                height: 550px;
                margin: none !important;
                padding: none !important;
            }
        </style>
    </head>
    <body>
        <div id="map"></div>
        <script type="text/javascript" src="<?= base_url('library/jquery-1.11.3.js') ?>"></script>
        <script>
            //==========---LAYER GROUP---==========//
            //layer group
            var Kondisi = L.layerGroup();
            
            //==========ICON==========//
            //caveIcon shadow
            var caveIcon = L.Icon.extend({
                options: {
                iconSize:     [40, 50],
                iconAnchor:   [25, 40],
                shadowAnchor: [0, 0],
                popupAnchor:  [-3, -76]
                }
            });
            
            // //warna kondisi goa
            var dua1 = new caveIcon({iconUrl: '../assets/map-marker-icon-blue.png'}),
                lima1 = new caveIcon({iconUrl: '../assets/map-marker-icon-green.png'}),
                satu51 = new caveIcon({iconUrl: '../assets/map-marker-icon-green.png'}),
                dua01 = new caveIcon({iconUrl: '../assets/yellow.png'}),
                tiga01 = new caveIcon({iconUrl: '../assets/yellow.png'}),
                empat01 = new caveIcon({iconUrl: '../assets/orange.png'}),
                lima01 = new caveIcon({iconUrl: '../assets/orange.png'}),
                els = new caveIcon({iconUrl: '../assets/map-marker-icon-red.png'}),
                kosong = new caveIcon({iconUrl: '../assets/map-marker-icon-default.png'});
            //==========END ICON==========//
            
            
            //==========---END LAYER GROUP---==========//
            
            //==========PUBLIC TOKEN=========//
            var token_map = 'pk.eyJ1IjoiZGFuYW5nb2ZmaWMiLCJhIjoiY2o3NjlkODFvMHlpeDMzczNhMTVjZnE0eCJ9.iW12sX94_cMv6P0ivbqbSA';
            
            //==========#MAP==========//
            
            
            
            //var url base layer
            var url = 'https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token='+token_map,
                url2 = 'https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token='+token_map,
                
            //attribution
            attr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>';
            
            //var untuk jenis base layer
            var sattelite = L.tileLayer(url, {id: 'mapbox.sattelite', attribution: attr}),
                streets  = L.tileLayer(url2, {id: 'mapbox.streets',   attribution: attr});
            
            //==========L.MAP==========//
            //=====declare mymap dari #map dengan set view jogja, zoom level 11=====//
            var mymap = L.map('map', {
                center: [-7.797068, 110.370529],
                zoom: 10,
                layers: [streets, Kondisi]
            });

            //var untuk load map tilelayer
            var load_map = L.tileLayer(url2, {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> ',
                maxZoom: 18,
                minZoom: 9,
                id: 'mapbox.streets',
                accessToken: token_map
            }).addTo(mymap);
            
            //=========LAYERING==========//
            
            var baseLayers = {
                    "Sattelite": sattelite,
                    "Streets": streets
                };
            
            //var overlays
            var overlays = {
                "Kondisi": Kondisi
            };
            
            //control layer berisi baselayer dan overlays
            L.control.layers(baseLayers, overlays).addTo(mymap); 
            //=========END LAYERING========//
            
            //=========KONDISI MARKER PENYESUAIAN GOA=========//
            
            <?php
            
            $n = 0;
                foreach($data->result() as $key){
                    
                    $lokasi = $key->id_lokasi;
                    // DATA HUJAN
                    $data_hujan = $this->db->select('*')
                                                ->from('data_alat, data_hujan')
                                                ->where('data_alat.id_lokasi', $lokasi)
                                                ->where('data_alat.id_alat=data_hujan.id_alat')
                                                ->order_by('nomor', 'DESC')
                                                ->limit('1')->get()->row();
                                                // var_dump($data_hujan);

                    $data_getaran = $this->db->select('g.frequensi AS frequensi, g.id_alat, g.tanggal, g.jam, g.nomor')
                                                ->from('data_getaran g')
                                                ->join('data_alat a', 'a.id_alat = g.id_alat')
                                                ->where('a.id_lokasi', $lokasi)
                                                ->order_by('nomor', 'DESC')
                                                ->limit(3)->get()->result();

                    $data_kelembaban = $this->db->select('k.kelembaban_tanah, k.id_alat, k.tanggal, k.jam, k.nomor')
                                                    ->from('data_kel_tanah k')
                                                    ->join('data_alat a', 'a.id_alat = k.id_alat')
                                                    ->where('a.id_lokasi', $lokasi)
                                                    ->order_by('nomor', 'DESC')
                                                    ->limit(3)->get()->result();

                    $data_suhu = $this->db->select('s.suhu_tanah, s.id_alat, s.tanggal, s.jam, s.nomor')
                                            ->from('data_su_tanah s')
                                            ->join('data_alat a', 'a.id_alat = s.id_alat')
                                            ->where('a.id_lokasi', $lokasi)
                                            ->order_by('nomor', 'DESC')
                                            ->limit(3)->get()->result();

                    ?>
                    var popup_ini<?=$n;?> = L.popup({maxWidth: '295'});
                    var frame<?=$n;?> = '<iframe src="<?=base_url('Map/pop_up/').$lokasi;?>" width="280" height="280"></iframe>';
                    popup_ini<?=$n;?>.setContent(frame<?=$n;?>);
                    <?php
                    if (($data_hujan->curah_hujan >= 0 && $data_hujan->curah_hujan < 21)) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: dua1}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        
                        <?php
                    } else if (($data_hujan->curah_hujan >= 21 && $data_hujan->curah_hujan < 51)) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: lima1}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        <?php
                    } else if ($data_hujan->curah_hujan >= 51 && $data_hujan->curah_hujan < 151) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: satu51}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        
                        <?php
                    } else if ($data_hujan->curah_hujan >= 151 && $data_hujan->curah_hujan < 201) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: dua01}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        
                        <?php
                    } else if ($data_hujan->curah_hujan >= 201 && $data_hujan->curah_hujan < 301) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: tiga01}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        
                        <?php
                    } else if ($data_hujan->curah_hujan >= 301 && $data_hujan->curah_hujan < 401) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: empat01}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        
                        <?php
                    } else if ($data_hujan->curah_hujan >= 401 && $data_hujan->curah_hujan < 501) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: lima01}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        
                        <?php
                    } else if($data_hujan->curah_hujan > 500) {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: els}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        
                        
                        <?php
                    } else {
                        ?>
                        L.marker([<?=$key->lat;?>,<?=$key->lon;?>],{icon: kosong}).bindPopup(popup_ini<?=$n;?>).addTo(Kondisi);
                        <?php
                    }
                    $n++;
                }
             ?>
            //=========END KONDISI MARKER PENYESUAIAN GOA=========//



            //==========END MARKER LOKASI==========//
            
            //==========LATLNG INFO==========//
            //var popup lat long onclick
            var popup_latlng = L.popup();
            function onMapClick(e) {
                popup_latlng
                    .setLatLng(e.latlng)
                    .setContent(
                    "Latitude: " + e.latlng.lat.toFixed(4)+
                    "<br>Longitude: " + e.latlng.lng.toFixed(4)
                        )
                    .openOn(mymap);
            }
            
            //action popup lat long onclick
            mymap.on('click', onMapClick);
            //==========END LATLNG INFO==========//
        </script>
    </body>
</html>