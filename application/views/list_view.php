<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('template/head'); ?>

</head>
<body>
<?php $this->load->view('template/nav'); ?>

	<div class="section no-pad-bot" id="index-banner">
	    <div class="container">
	        <div class="row">
	            <div class="col s12 m12">
	                <ul class="collapsible">
	                	<?php
	                	foreach ($list_lokasi as $row) {
                            $id_lokasi = $row->id_lokasi;
	                		$hujan = $this->list->get_hujan1($id_lokasi)->row();
	                		$getaran = $this->list->get_getaran1($id_lokasi)->row();
	                		$kelembaban = $this->list->get_kelembaban1($id_lokasi)->row();
	                		$suhu = $this->list->get_suhu1($id_lokasi)->row();
                            if(isset($hujan)):
                                $curah_hujan = $hujan->curah_hujan;
    	                		$result_hujan = '<span class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Curah Hujan">
                                                            <i class="wi wi-raindrop"></i>&nbsp;
                                                            '.$curah_hujan.'
                                                        </span>&nbsp;';
                                                        
                            endif;
                            if(isset($getaran)):
                                $frequensi = $getaran->frequensi;
                                $result_getaran = '<span class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Getaran Tanah"><i class="wi wi-earthquake"></i>&nbsp;
                                                        ' . $frequensi . '</span>&nbsp;';
                            endif;
                            if(isset($kelembaban)):
                                $kelembaban_tanah = $kelembaban->kelembaban_tanah;
    							$result_kelembaban = '<span class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Kelembaban Tanah"><i class="wi wi-flood"></i>&nbsp;
                                                        '.$kelembaban_tanah.'</span>&nbsp;';
                            endif;
                            if(isset($suhu)):
                                $suhu_tanah = $suhu->suhu_tanah;
    							$result_suhu = '<span class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Suhu Tanah">
                                                        <i class="wi wi-thermometer-internal"></i>&nbsp;
                                                        '.$suhu_tanah.'</span>&nbsp;';
                            endif;
                            if(($curah_hujan<101) || ($frequensi < 21) || ($kelembaban_tanah > 30 && $kelembaban_tanah< 51) || ($suhu_tanah > 23 && $suhu_tanah < 36)):
                                $hex = "#32bc00";
                                $level = "Normal";
                            elseif(($curah_hujan > 100 && $curah_hujan < 301) || ($frequensi > 20 && $frequensi < 51) || (($kelembaban_tanah > 0 && $kelembaban_tanah < 31)||($kelembaban_tanah > 50 && $kelembaban_tanah < 71)) || ($suhu_tanah > 18 && $suhu_tanah < 24)):
                                $hex = "#c5cc00";
                                $level = "Waspada";
                            elseif(($curah_hujan > 300 && $curah_hujan < 501) || ($frequensi > 50 && $frequensi < 71) || ($kelembaban_tanah > 70 && $kelembaban_tanah < 90) || ($suhu_tanah > 10 && $suhu_tanah < 19)):
                                $hex = "#cc7300";
                                $level = "Siaga";
                            elseif(($curah_hujan > 500) || ($frequensi > 70) || ($kelembaban_tanah > 89) || ($suhu_tanah < 11)):
                                $hex = "#c42700";
                                $level = "Awas";
                            endif;
	                		?>
	                		<li>
                                <div class="collapsible-header">
                                    <i class="material-icons circle tooltipped" data-position="bottom" data-delay="50" data-tooltip="Kondisi terkini adalah" style="background-color: <?=$hex?>; color: white;">directions_walk</i><?= $row->lokasi; ?>&nbsp;
                                    <span class="hide-on-med-and-up lighten-2" style="color: <?= $hex ?>">(Kondisi terini adalah <?= $level; ?>)</span>
                                </div>
                                <div class="collapsible-body">
                                    <div class="col s12">
                                        <div class="left">
                                            <?php
                                            //==========Hujan==========//
                                            if(isset($result_hujan)):
                                                echo $result_hujan;
                                            endif;
                                            if(isset($result_getaran)):
                                                echo $result_getaran;
                                            endif;
                                            if(isset($result_suhu)):
                                                echo $result_suhu;
                                            endif;
                                            if(isset($result_kelembaban)):
                                                echo $result_kelembaban;
                                            endif;
                                            //==========Suhu Indoor==========//
                                            // <!--  using celcius degree -->
                                            
                                            // $detail right
                                            // echo $detail_right;
                                            ?>
                                        </div>
                                        <div class="right">
                                            <a href="<?=base_url('detail/graph/').$id_lokasi;?>" class="waves-effect waves-light"><i class="material-icons tooltipped" data-position="bottom" data-delay="50" data-tooltip="Graph Detail <?=$row->lokasi;?>">poll</i></a>
                                        </div>
                                    </div>
                                </div>
                                        
                                    
                            </li>
	                		<?php
	                	}
	                	?>
	                </ul>
	            </div>
	        </div>
	    </div>
	</div>
	<?php login_modal();?>

<?php $this->load->view('template/jsfoot'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        M.AutoInit();
      });
</script>
</body>
</html>