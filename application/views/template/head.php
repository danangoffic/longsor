    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Landslide Monitoring</title>
    <meta name="token" content="<?=md5(uniqid(rand(), true));?>">
    <meta name="description" content="Rain and Soil Monitoring - Indonesia, Yogyakarta" />
    <meta NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    <link rel="shortcut icon" href="<?= base_url('admin/') ?>assets/images/logo/logo.png" type="image/x-icon">
    <link rel="icon" href="<?= base_url('admin/') ?>assets/images/logo/logo.png" type="image/x-icon">
    <meta content="" name="author" />
    <meta property="og:site_name" content="Landslide Monitoring - Indonesia, Yogyakarta">
    <meta property="og:description" content="Rain and Soil Monitoring - Indonesia, Yogyakarta" />
    <meta property="og:type" content="website"/>
    <meta name="theme-color" content="#3d5afe">
    <link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" type="text/css" href="http://103.195.90.35:83/library/plugins/morris/morris.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?=base_url('library/materialize/')?>/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('library/plugins/morris/morris.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('library/weather-icons-master/css/weather-icons.css')?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('library/weather-icons-master/css/weather-icons-wind.css')?>">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
          integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
          crossorigin=""></script>
    <style>
        #map{
            height: 550px;
        }
    </style>