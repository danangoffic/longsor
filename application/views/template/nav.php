    <nav id="nav" class="blue accent-4" role="navigation">
        <div class="nav-wrapper">
            <a href="<?=base_url()?>" class="brand-logo center show-on-medium-and-up hide-on-med-and-down" style="font-size:18px;">LANDSLIDE MONITORING</a>
            <a href="<?=base_url()?>" class="brand-logo center show-on-small hide-on-med-and-up" style="font-size:14px;">LANDSLIDE MONITORING</a>
            <!-- <a href="<?=base_url()?>#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a> -->
            <ul class="left hide-on-med-and-down">
                <li <?=active('home', $tt)?>><a href="<?=base_url('')?>">Home</a></li>
                <li <?=active('list', $tt)?>><a href="<?=base_url('/List_land')?>">Daftar Lokasi</a></li>
                <li <?=active('maplocation', $tt)?>><a href="<?=base_url('/Map')?>" >Map Location</a></li>
                <?php if($tt=="highlight") echo "<li ". active('highlight', $tt) ."><a href='".base_url('detail/highlight/'.$id.'/'.$lokasi)."' >".$lokasi.$id."</a></li>";?>
            </ul>
            <ul class="right hide-on-med-and-down">
                <li><a href="admin" class="modal-trigger">Login</a></li>
            </ul>
            <ul class="sidenav" id="nav-mobile">
                <li <?=active('home', $tt)?>><a href="<?=base_url('')?>">Home</a></li>
                <li <?=active('list', $tt)?>><a href="<?=base_url('/List_land')?>">Daftar Lokasi</a></li>
                <li <?=active('maplocation', $tt)?>><a href="<?=base_url('/Map')?>" >Map Location</a></li>
                <?php if($tt=="highlight") echo "<li ". active('highlight', $tt) ."><a href='".base_url('detail/highlight/'.$id.'/'.$lokasi)."' >".$lokasi.$id."</a></li>";?>
                <li><a href="admin" class="modal-trigger">Login</a></li>
            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>
