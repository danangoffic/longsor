<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('template/head'); ?>
<style>
            @page {
                size: A4;
                margin: 0;
            }

            @media print {
                body * {
                    visibility: hidden;
                }
                #section-to-print, #section-to-print * {
                    visibility: visible;
                }
                #section-to-print {
                    display: block;
                    width: 21cm;
                    height: 29.5cm;
                    margin: 0;
                    position: absolute;
                    left: 0;
                    top: 0;
                }
                .card-action, .card-action * {
                    visibility: hidden !important;
                }                
            }

        </style>
</head>
<body>
<?php $this->load->view('template/nav'); ?>

	<div class="section no-pad-bot" id="index-banner">
	    <div class="container">
	        <div class="row">
	            <div class="col s12 m12">
	                <div class="row">
	                    <div class="col s12">
	                       <div class="card">
	                            <div class="card-content">
	                                <div class="card-title judul"></div>
	                                <p class="isi"></p>
	                            </div>
	                            <div class="card-action">
	                                <a id="details" class="waves-effect waves-light btn" style="background-color: #007fff;" target="_blank">details</a>
	                                <div class="chip right red nn" style="color:white"></div>
	                            </div>
	                        </div> 
	                    </div>
	                </div>
	                
	            </div>
	        </div>
	        <div class="row">
	            <div class="col s12 m12">
	                <div id="data-list">

	                    <nav class="indigo accent-3">
	                        <div class="nav-wrapper">
	                          
	                            <div class="input-field">
	                              <input id="search" type="search" class="search" required placeholder="Cari Nama Tempat Rawan Longsor [ex: Karang Tengah], Tanggal [ex: HARI || TANGGAL || TAHUN || NAMA_BULAN], Level..">
	                              <label class="label-icon" for="search"><i class="material-icons">search</i></label>
	                            </div>
	                          
	                        </div>
	                    </nav>
	                    <ul class="list collection left-align" id="list">
	                        <li class="collection-item left-align" style="z-index: 10;">

	                        </li>
	                    </ul>
	                    <ul class="pagination pager" id="pager" >

	                    </ul>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<?php login_modal();?>

<?php $this->load->view('template/jsfoot'); ?>
<script type="text/javascript">
	
</script>
</body>
</html>