<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function index()
    {
        echo 'Access Denied!';
    }

    private function checkOp($in){
        if(empty($in)) {
            $s = new stdClass();
            $s->status = false;
            $s->message = "Access Denied!";
            printJSON($s);
        }
    }

    function signin(){
        $this->checkOp($this->input->post());
        $this->load->model('ma');
        $res = $this->ma->login($this->input->post());
        printJSON($res);
    }

    function profil(){
        $this->checkOp($this->input->post());
        $this->load->model('ma');
        $res = $this->ma->getProfil($this->input->post());
        printJSON($res);
    }

    function profilKPU(){
        $this->checkOp($this->input->post());
        $this->load->model('ma');
        $res = $this->ma->getProfilKPU($this->input->post());
        printJSON($res);
    }
}
