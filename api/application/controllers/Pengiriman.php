<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengiriman extends CI_Controller {
    public function index()
    {
        echo 'Access Denied!';
    }

    private function checkOp($in){
        if(empty($in)) {
            $s = new stdClass();
            $s->status = false;
            $s->message = "Access Denied!";
            printJSON($s);
        }
    }

    function getList(){
        $this->checkOp($this->input->post());
        $this->load->model('mp');
        $res = $this->mp->getPengirimanList($this->input->post());
        printJSON($res);
    }

    function getListDetail(){
        $this->checkOp($this->input->post());
        $this->load->model('mp');
        $res = $this->mp->getPengirimanListDetail($this->input->post());
        printJSON($res);
    }

    //Belum Digunakan
    function getListFlag(){
        $this->checkOp($this->input->post());
        $this->load->model('mp');
        $res = $this->mp->getListFlag($this->input->post());
        printJSON($res);
    }

    function cekArmada(){
        $this->checkOp($this->input->post());
        $this->load->model('mp');
        $res = $this->mp->cekDetailArmada($this->input->post());
        printJSON($res);
    }

    function checkFlag(){
        $this->checkOp($this->input->post());
        $this->load->model('mp');
        $res = $this->mp->cekFlag($this->input->post());
        printJSON($res);
    }

    function updateKirim(){
        $this->checkOp($this->input->post());
        $this->load->model('mp');
        $res = $this->mp->updateKirim($this->input->post());
        printJSON($res);
    }

    function updateGeo(){
        //$this->checkOp($this->input->post());
        $this->load->model('mp');
        $input = json_decode(json_encode(json_decode(file_get_contents('php://input'), true)[0]));
        $input->kode = $_SERVER['HTTP_IDPENGIRIMAN'];

        $res = $this->mp->updateGeo($input);
        printJSON($res);
    }

    function updateFlag(){
        $this->checkOp($this->input->post());
        $this->load->model('mp');
        $in = a2o($this->input->post());
	$lokasi = $this->reverseGeo($in->latitude, $in->longitude);
        $res = $this->mp->updateFlag($this->input->post(), $lokasi);
        printJSON($res);
    }

    function upload($arg1, $arg2, $arg3){
        $this->load->model('mp');
        $res = $this->mp->uploadPhoto($arg1,$arg2,$arg3);
        printJSON($res);
    }

    function reverseGeo($lat, $lng){
        //$lat='-7.3160227';  //latitude
        //$lng='112.7490212';  //longitude

        //$lat='-7.2421854';  //latitude
        //$lng='112.7211598';  //longitude

	//$lat='7.234054';
	//$lng='112.716225';
        $endpoint="http://maps.googleapis.com/maps/api/geocode/json?latlng=".trim($lat).",".trim($lng)."&sensor=false";

        $raw=@file_get_contents($endpoint);
        $json_data=json_decode($raw);

        if ($json_data->status=="OK") {
            $fAddress=explode(",", $json_data->results[count($json_data->results)-2]->formatted_address);  //this is human readable address --> getting province name
            //var_dump($json_data->results);  //dumping result
            //printJSON($json_data->results[1]->formatted_address);
	    $r = $json_data->results;
	    $intype = array('street_number','route');
	    for($i=0;$i<count($r);$i++){
		if(in_array($r[$i]->address_components[0]->types[0], $intype)){
		    $addd = $r[$i]->formatted_address;
		}
	    }
	    return $addd;
	    //printJSON($addd);
	    //printJSON($json_data->results);
        } else {
            //if no result found, status would be ZERO_RESULTS
            //echo $json_data->status;
	    return "Tidak Diketahui";
        }
    }
}
