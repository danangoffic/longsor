<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    public function index()
    {
        echo 'Access Denied!';
    }

    private function checkOp($in){
        if(empty($in)) {
            $s = new stdClass();
            $s->status = false;
            $s->message = "Access Denied!";
            printJSON($s);
        }
    }

    function getList(){
        $this->checkOp($this->input->post());
        $this->load->model('ml');
        $res = $this->ml->getLaporanList($this->input->post());
        printJSON($res);
    }

    function getListDetail(){
        $this->checkOp($this->input->post());
        $this->load->model('ml');
        $res = $this->ml->getLaporanListDetail($this->input->post());
        printJSON($res);
    }

    function getKPUList(){
        $this->checkOp($this->input->post());
        $this->load->model('ml');
        $res = $this->ml->getLaporanKPUList($this->input->post());
        printJSON($res);
    }

    function getKPUDetail(){
        $this->checkOp($this->input->post());
        $this->load->model('ml');
        $res = $this->ml->getLaporanKPUDetail($this->input->post());
        printJSON($res);
    }
}
