<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi extends CI_Controller {
    public function index()
    {
        echo 'Access Denied!';
    }

    private function checkOp($in){
        if(empty($in)) {
            $s = new stdClass();
            $s->status = false;
            $s->message = "Access Denied!";
            printJSON($s);
        }
    }

    function getList(){
        $this->checkOp($this->input->post());
        $this->load->model('mv');
        $res = $this->mv->getVerifikasiList($this->input->post());
        printJSON($res);
    }

    function getListDetail(){
        $this->checkOp($this->input->post());
        $this->load->model('mv');
        $res = $this->mv->getVerifikasiListDetail($this->input->post());
        printJSON($res);
    }

    function scanPack(){
        $this->checkOp($this->input->post());
        $this->load->model('mv');
        $res = $this->mv->getPack($this->input->post());
        printJSON($res);
    }

    function updatePack(){
        $this->checkOp($this->input->post());
        $this->load->model('mv');
        $res = $this->mv->updatePack($this->input->post());
        printJSON($res);
    }

    function updateFlag(){
        $this->checkOp($this->input->post());
        $this->load->model('mv');
        $res = $this->mv->updateFlag($this->input->post());
        printJSON($res);
    }
}
