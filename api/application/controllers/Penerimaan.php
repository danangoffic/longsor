<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan extends CI_Controller {
    public function index()
    {
        echo 'Access Denied!';
    }

    private function checkOp($in){
        if(empty($in)) {
            $s = new stdClass();
            $s->status = false;
            $s->message = "Access Denied!";
            printJSON($s);
        }
    }

    function getList(){
        $this->checkOp($this->input->post());
        $this->load->model('mt');
        $res = $this->mt->getPenerimaanList($this->input->post());
        printJSON($res);
    }

    function getListDetail(){
        $this->checkOp($this->input->post());
        $this->load->model('mt');
        $res = $this->mt->getPenerimaanListDetail($this->input->post());
        printJSON($res);
    }

    function scanPack(){
        $this->checkOp($this->input->post());
        $this->load->model('mt');
        $res = $this->mt->getPack($this->input->post());
        printJSON($res);
    }

    function updatePack(){
        $this->checkOp($this->input->post());
        $this->load->model('mt');
        $res = $this->mt->updatePack($this->input->post());
        printJSON($res);
    }

    function updateFlag(){
        $this->checkOp($this->input->post());
        $this->load->model('mt');
        $res = $this->mt->updateFlag($this->input->post());
        printJSON($res);
    }
}
