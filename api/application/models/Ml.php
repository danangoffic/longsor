<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ml extends CI_Model {
    function getLaporanList($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "select * from (select * from (select kode_pengiriman, nomor_surat from pengantar_barang) ta natural join (select kode_pengiriman, kode_packing from detail_pengiriman group by kode_pengiriman) tb natural join (select IdPengemudi, kode_pengiriman, tgl_pengiriman, status from pengiriman) tc natural join (select kode_packing, kpu_tujuan, kpu_pemesan from packing group by kode_packing) td) te left join serah_terima_kpud tf on te.kode_pengiriman=tf.kode_pengiriman where IdPengemudi='$in->id'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            foreach ($res1->result() as $r1){
                if(empty($r1->waktu)) $r1->tgl_terima = '-';
                else $r1->tgl_terima = substr($r1->waktu,0,10);
                if($r1->status>1) $r1->status = 'TERKIRIM';
                else $r1->status = 'BELUM TERKIRIM';
                $data[] = $r1;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
    }

    function getLaporanListDetail($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "SELECT jenis_armada, no_armada, flag FROM detail_armada_pengiriman where kode_pengiriman='$in->kode'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            $row = $res1->row();
            //$data[] = $row;
            //$s->data = $data;
            $s->status = true;
            $s->penerima = 'armada';
            $s->jenis = $row->jenis_armada;
            $s->nomor = $row->no_armada;
            $s->flag = $row->flag;

            $sql2 = "SELECT jenis_armada, no_armada, check_point FROM detail_armada_pengiriman where kode_pengiriman='$in->kode'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    $data[] = $r;
                }
                $s->data = $data;
            }
        } else {
            $sql2 = "select * from (select * from (select kode_pengiriman, nomor_surat from pengantar_barang) ta natural join (select kode_pengiriman, kode_packing from detail_pengiriman group by kode_pengiriman) tb natural join (select IdPengemudi, kode_pengiriman, tgl_pengiriman, status from pengiriman) tc natural join (select idKpuTujuan, kode_packing, kpu_tujuan, kpu_pemesan from packing group by kode_packing) td natural join (select IdMasterKpu as idKpuTujuan, alamat_kantor from master_kpu) te natural join (select IdMasterKpu as idKpuTujuan, nama_pegawai, no_kontak from penerima_logistik) tf natural join (select kode_pengiriman, Flag from serah_terima_kpud) tg) th left join (select kode_pengiriman, Status, waktu, gambar, BeritaAcara, latitude, longitude, namalokasi from serah_terima_kpud) ti on th.kode_pengiriman=ti.kode_pengiriman where kode_packing='$in->kode'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    if(empty($r->waktu)) {
                        $r->namalokasi = '-';
                        $r->tgl_terima = '-';
                        $r->jam_terima = '-';
                    } else {
                        $r->tgl_terima = substr($r->waktu,0,10);
                        $r->jam_terima = substr($r->waktu,11);
                    }
                    if($r->status>1) $r->status = 'TERKIRIM';
                    else $r->status = 'BELUM TERKIRIM';
                    $data[] = $r;
                }
                $s->status = true;
                $s->penerima = 'kpud';
                $s->data = $data;
            }
        }

        return $s;
    }

    function getLaporanKPUList($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "select * from (select * from (select kode_pengiriman, nomor_surat from pengantar_barang) ta natural join (select kode_pengiriman, kode_packing from detail_pengiriman group by kode_pengiriman) tb natural join (select IdPengemudi, kode_pengiriman, tgl_pengiriman, status from pengiriman) tc natural join (select idKpuTujuan, kode_packing, kpu_tujuan, kpu_pemesan from packing group by kode_packing) td) te left join serah_terima_kpud tf on te.kode_pengiriman=tf.kode_pengiriman where idKpuTujuan='$in->id'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            foreach ($res1->result() as $r1){
                if(empty($r1->waktu)) $r1->tgl_terima = '-';
                else $r1->tgl_terima = substr($r1->waktu,0,10);
                if($r1->status>1) $r1->status = 'TERKIRIM';
                else $r1->status = 'BELUM TERKIRIM';
                $data[] = $r1;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
    }

    function getLaporanKPUDetail($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "SELECT jenis_armada, no_armada, flag FROM detail_armada_pengiriman where kode_pengiriman='$in->kode'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            $row = $res1->row();
            //$data[] = $row;
            //$s->data = $data;
            $s->status = true;
            $s->penerima = 'armada';
            $s->jenis = $row->jenis_armada;
            $s->nomor = $row->no_armada;
            $s->flag = $row->flag;

            $sql2 = "SELECT jenis_armada, no_armada, check_point FROM detail_armada_pengiriman where kode_pengiriman='$in->kode'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    $data[] = $r;
                }
                $s->data = $data;
            }
        } else {
            $sql2 = "select * from (select * from (select kode_pengiriman, nomor_surat from pengantar_barang) ta natural join (select kode_pengiriman, kode_packing from detail_pengiriman group by kode_pengiriman) tb natural join (select IdPengemudi, kode_pengiriman, tgl_pengiriman, status from pengiriman) tc natural join (select idKpuTujuan, kode_packing, kpu_tujuan, kpu_pemesan from packing group by kode_packing) td natural join (select IdMasterKpu as idKpuTujuan, alamat_kantor from master_kpu) te natural join (select IdMasterKpu as idKpuTujuan, nama_pegawai, no_kontak from penerima_logistik) tf natural join (select kode_pengiriman, Flag from serah_terima_kpud) tg) th left join (select kode_pengiriman, Status, waktu, gambar, BeritaAcara, latitude, longitude, namalokasi from serah_terima_kpud) ti on th.kode_pengiriman=ti.kode_pengiriman where kode_packing='$in->kode'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    if(empty($r->waktu)) {
                        $r->namalokasi = '-';
                        $r->tgl_terima = '-';
                        $r->jam_terima = '-';
                    } else {
                        $r->tgl_terima = substr($r->waktu,0,10);
                        $r->jam_terima = substr($r->waktu,11);
                    }
                    if($r->status>1) $r->status = 'TERKIRIM';
                    else $r->status = 'BELUM TERKIRIM';
                    $data[] = $r;
                }
                $s->status = true;
                $s->penerima = 'kpud';
                $s->data = $data;
            }
        }

        return $s;
    }
}
