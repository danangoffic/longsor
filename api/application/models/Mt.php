<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mt extends CI_Model {
    function getPenerimaanList($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        //$sql1 = "SELECT * FROM kpu_pemesan as p, detail_pemesanan as d, jenis_barang_logistik as j where p.kode_pemesanan=d.kode_pemesanan and p.IdMasterKpu='$in->id' and d.kode_barang=j.kode_barang";
        //$sql1 = "SELECT c.kode_pemesanan, d.kode_pengiriman, p.kode_vendor, no_surat_pemesanan, idKpuTujuan, kpu_tujuan, kpu_pemesan, if(status=2,'LENGKAP','BELUM LENGKAP') as status  FROM pengiriman as p, detail_pengiriman as d, packing as c, pemesanan_logistik as pl where p.kode_pengiriman=d.kode_pengiriman and d.kode_packing=c.kode_packing and pl.kode_vendor=p.kode_vendor and pl.kode_pemesanan=c.kode_pemesanan group by c.idKpuTujuan";
        //$sql1 = "select *, if(Status=1,'TERVERIFIKASI','BELUM VERIFIKASI') as status_pesan from (SELECT kode_pengiriman, c.kode_pemesanan, no_surat_pemesanan, idKpuTujuan, kpu_tujuan, kpu_pemesan, Status FROM packing as c, pemesanan_logistik as pl, serah_terima_kpud as sk where sk.kode_kpu=idKpuTujuan and pl.kode_pemesanan=c.kode_pemesanan and idKpuTujuan='$in->id' group by pl.kode_pemesanan) ta natural join (select max(id) as id from status_pemesanan group by kode_pemesanan) tb natural join (select id, kode_pemesanan, tanggal from status_pemesanan) tc";
        $sql1 = "select *, if(Status=1,'TERVERIFIKASI','BELUM VERIFIKASI') as status_pesan from (SELECT dp.kode_pengiriman, c.kode_pemesanan, no_surat_pemesanan, idKpuTujuan, kpu_tujuan, kpu_pemesan, Status FROM packing as c, pemesanan_logistik as pl, serah_terima_kpud as sk, (select * from detail_pengiriman group by kode_pengiriman) dp where sk.kode_kpu=idKpuTujuan and sk.kode_pengiriman=dp.kode_pengiriman and pl.kode_pemesanan=c.kode_pemesanan and dp.kode_packing=c.kode_packing and idKpuTujuan='$in->id' group by pl.kode_pemesanan) ta natural join (select max(id) as id from status_pemesanan group by kode_pemesanan) tb natural join (select id, kode_pemesanan, tanggal from status_pemesanan) tc";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            foreach ($res1->result() as $r1){
                $sql2 = "select *, count(kode_barang) as jpack from (SELECT k.kode_packing, p.kode_barang, jumlah, nama_barang FROM detail_pemesanan as d, jenis_barang_logistik as j, packing as k, detail_packing as p where d.kode_barang=j.kode_barang AND j.kode_barang=p.kode_barang and d.kode_pemesanan=k.kode_pemesanan and k.kode_packing=p.kode_packing and d.kode_pemesanan='$r1->kode_pemesanan' group by IdDetailPacking) pa group by kode_barang";
                $res2 = $this->db->query($sql2);
                $num2 = $res2->num_rows();
                $data2 = [];
                if($num2>0){
                    foreach ($res2->result() as $r2){
                        $data2[] = $r2;
                    }
                    $r1->barang = $data2;
                }
                if($r1->status_pesan=='BELUM VERIFIKASI') $data[] = $r1;
            }
            $s->status = true;
        }

        $s->data = $data;
        return $s;
    }

    function getPenerimaanListDetail($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data1 = [];
        $data2 = [];

        $sql1 = "SELECT kode_packing, count(kode_packing) as jpack FROM detail_pengiriman where kode_packing='$in->kode'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            $row = $res1->row();
            $s->kodepack = $row->kode_packing;
            $s->jumlahpack = $row->jpack;
            $s->status = true;
        } else {
            $s->kodepack = '-';
            $s->jumlahpack = '-';
        }

        if(isset($s->kodepack)){
            $sql2 = "select sum(jumlah_logistik) as jumlahbarang from detail_packing where kode_packing='$s->kodepack'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                $row = $res2->row();
                $s->jumlahbarang = $row->jumlahbarang;
            } else {
                $s->jumlahbarang = '-';
            }

            $sql3 = "select sum(jumlah_logistik) as jumlahbarang from detail_packing where kode_packing='$s->kodepack'";
            $res3 = $this->db->query($sql3);
            $num3 = $res3->num_rows();
            if($num3>0){
                $row = $res3->row();
                $s->jumlahbarang = $row->jumlahbarang;
            } else {
                $s->jumlahbarang = '-';
            }

            $sql4 = "select status, sum(jumlah_logistik) as vbarang, count(kode_barang) as vpack from detail_packing where kode_packing='$s->kodepack' and status=2 group by status";
            $res4 = $this->db->query($sql4);
            $num4 = $res4->num_rows();
            if($num4>0){
                $row = $res4->row();
                $s->vbarang = $row->vbarang;
                $s->vpack = $row->vpack;
            } else {
                $s->vbarang = '0';
                $s->vpack = '0';
            }

            $sql5 = "select kode_barang, nama_barang, jumlah_logistik, urutan_packing, status from detail_packing NATURAL join jenis_barang_logistik where kode_packing='$s->kodepack'";
            $res5 = $this->db->query($sql5);
            $num5 = $res5->num_rows();
            if($num5>0){
                foreach ($res5->result() as $r){
                    if(!isset($jp[$r->kode_barang])) $jp[$r->kode_barang] = 0;
                    $jp[$r->kode_barang] += 1;
                    if($r->status == 1) $data1[] = $r;
                    else $data2[] = $r;
                }

                for($i=0; $i<count($data1); $i++){
                    $data1[$i]->jpack = $jp[$data1[$i]->kode_barang];
                }
                for($i=0; $i<count($data2); $i++){
                    $data2[$i]->jpack = $jp[$data2[$i]->kode_barang];
                }
                $s->listv = $data2;
                $s->listb = $data1;
            }
        }

        return $s;
    }

    function getPack($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql = "SELECT 1 FROM detail_packing where kode_packing_barang='$in->kode' and urutan_packing='$in->urut'";
        $res = $this->db->query($sql);
        $num = $res->num_rows();
        if($num>0){
            $s->status = true;
        }

        return $s;
    }

    function updatePack($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;

        $sql1 = "UPDATE detail_packing SET status='2' WHERE kode_packing_barang='$in->kode' and urutan_packing='$in->urut'";
        $this->db->query($sql1);
        $done1 = ($this->db->affected_rows() != 1) ? false : true;

        if($done1){
            $sql2 = "SELECT IdDetailPacking as id FROM detail_packing where kode_packing_barang='$in->kode' and urutan_packing='$in->urut'";
            $res2 = $this->db->query($sql2);
            $row = $res2->row();
            $iddp = $row->id;

            $date = date('Y-m-d');
            $sql3 = "insert into status_packing values ('','$in->kodepack','$iddp','$date','4')";
            $this->db->query($sql3);
            $done2 = ($this->db->affected_rows() != 1) ? false : true;

            if($done2){
                $s->status = true;
            }
        }

        return $s;
    }

    function updateFlag($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];
        $date = date('Y-m-d');
        $time = date('Y-m-d H:i:s');

        $sql1 = "SELECT * FROM serah_terima_kpud where kode_pengiriman='$in->kode' and Flag='$in->flag'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            $sql2 = "SELECT * FROM detail_pengiriman where kode_pengiriman='$in->kode'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    $id = $r->IdDetailPacking;
                    $pack = $r->kode_packing;

                    $sql3 = "insert into status_packing values ('','$pack','$id','$date','6')";
                    $this->db->query($sql3);
                }
            }

            $sql4 = "insert into status_pengiriman values ('','$in->kode','$date','6')";
            $this->db->query($sql4);

            $sql5 = "UPDATE `serah_terima_kpud` SET `status` = '1', id_user='$in->user', waktu='$time' WHERE kode_pengiriman = '$in->kode'";
            $this->db->query($sql5);

            $sql6 = "UPDATE `detail_armada_pengiriman` SET `status` = '2' WHERE kode_pengiriman = '$in->kode'";
            $this->db->query($sql6);

            $sql7 = "UPDATE `pengiriman` SET `status` = '2' WHERE kode_pengiriman = '$in->kode'";
            $this->db->query($sql7);
            $s->status = true;
        }

        return $s;
    }
}
