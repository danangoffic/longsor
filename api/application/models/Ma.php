<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ma extends CI_Model {
    function login($input){
        $in = a2o($input);
        $pass = md5($in->password);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "SELECT * FROM detail_armada_pengiriman where pic='$in->username'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1 == 0){
            $sql2 = "SELECT * FROM `pengemudi` where no_ktp='$in->username' and password='$pass'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r2){
                    $data[] = $r2;
                }
                $s->kodelogin = 1;
                $s->status = true;
            } else {
                $sql3 = "SELECT * FROM `user` as u, penerima_logistik as l where u.nip=l.nip and u.username='$in->username' and password='$pass'";
                $res3 = $this->db->query($sql3);
                $num3 = $res3->num_rows();

                if($num3>0){
                    foreach ($res3->result() as $r3){
                        $data[] = $r3;
                    }
                    $s->kodelogin = 2;
                    $s->status = true;
                }
            }
        } else {
            $sql2 = "SELECT * FROM `pengemudi` where no_ktp='$in->username' and password='$pass'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r2){
                    $data[] = $r2;
                }
                $s->kodelogin = 3;
                $s->status = true;
            }
        }

        $s->data = $data;
        return $s;
    }

    function getProfil($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;

        $sql1 = "SELECT * FROM pengemudi as p, data_vendor as v where p.kode_vendor=v.kode_perusahaan and p.IdPengemudi='$in->id'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        $data = [];
        if($num1>0){
            foreach ($res1->result() as $r){
                $data[] = $r;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
    }

    function getProfilKPU($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;

        $sql1 = "SELECT l.nip, nama_pegawai, no_kontak, email, k.wilayah, nama_satker, nama_provinsi, foto  FROM user as u, penerima_logistik as l, master_kpu as k, master_provinsi p where u.nip=l.nip and l.IdMasterKpu=k.IdMasterKpu and k.kode_provinsi=p.kode_provinsi and l.nip='$in->id'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        $data = [];
        if($num1>0){
            foreach ($res1->result() as $r){
                $data[] = $r;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
    }
}
