<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mp extends CI_Model {
    function getPengirimanList($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "select * from (select kode_pengiriman, nomor_surat from pengantar_barang) ta natural join (select kode_pengiriman, kode_packing from detail_pengiriman group by kode_pengiriman) tb natural join (select IdPengemudi, kode_pengiriman, tgl_pengiriman, status from pengiriman) tc natural join (select kode_packing, kpu_tujuan, kpu_pemesan from packing group by kode_packing) td where IdPengemudi='$in->id'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            foreach ($res1->result() as $r1){
                if($r1->status==1) $data[] = $r1;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
    }

    function getPengirimanListDetail($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "select * from (select kode_pengiriman, nomor_surat from pengantar_barang) ta natural join (select kode_pengiriman, kode_packing from detail_pengiriman group by kode_pengiriman) tb natural join (select IdPengemudi, kode_pengiriman, tgl_pengiriman, status as statuskirim from pengiriman) tc natural join (select idKpuTujuan, kode_packing, kpu_tujuan, kpu_pemesan from packing group by kode_packing) td natural join (select IdMasterKpu as idKpuTujuan, alamat_kantor from master_kpu) te where kode_packing='$in->kode'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            $row = $res1->row();
            $s->nomorsurat = $row->nomor_surat;
            $s->kodepengiriman = $row->kode_pengiriman;
            $s->kpupemesan = $row->kpu_pemesan;
            $s->kputujuan = $row->kpu_tujuan;
            $s->alamat = $row->alamat_kantor;
            $s->tgl = $row->tgl_pengiriman;
            $s->kodepack = $row->kode_packing;
            $s->statuskirim = $row->statuskirim;

            $sql2 = "select kode_barang, nama_barang, sum(jumlah_logistik) as jumlah, count(kode_barang) as jpack from detail_packing NATURAL join jenis_barang_logistik where kode_packing='$s->kodepack' group by kode_barang";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    $data[] = $r;
                }
                $s->status = true;
                $s->data = $data;
            }
        }

        return $s;
    }

    function updateKirim($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $date = date('Y-m-d');

        $sql1 = "SELECT * FROM detail_packing where kode_packing='$in->pack'";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            $row = $res1->row();
            $iddp = $row->IdDetailPacking;
            $sql2 = "insert into status_packing values ('','$in->pack','$iddp','$date','5')";
            $this->db->query($sql2);

            $sql3 = "insert into status_pengiriman values ('','$in->kode','$date','5')";
            $this->db->query($sql3);
            $done = ($this->db->affected_rows() != 1) ? false : true;

            if($done) $s->status = true;

            $sql4 = "select * from (select kode_packing, idKpuTujuan from packing) ta natural join (select kode_packing, kode_pengiriman from detail_pengiriman where kode_pengiriman='$in->kode' group by kode_pengiriman) tb natural join (select IdMasterKpu as idKpuTujuan, latitude_kantor, longitude_kantor from master_kpu) tc";
            $res4 = $this->db->query($sql4);
            $row = $res4->row();
            $s->data = $row;
        }

        return $s;
    }

    function updateGeo($in){
        $s = new stdClass();
        $s->status = false;
        $date = date('Y-m-d H:i:s', substr($in->time,0,10));
        if(empty($in->bearing)) $in->bearing = 0;

        $sql = "insert into tracking_lokasi values ('','$in->latitude', '$in->longitude','$in->bearing','$in->speed','$in->kode', '$date')";
        $this->db->query($sql);
        $done = ($this->db->affected_rows() != 1) ? false : true;
        if($done){
            $s->status = true;
        }

        return $s;
    }

    function cekDetailArmada($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "SELECT jenis_armada, no_armada, flag FROM detail_armada_pengiriman where kode_pengiriman='$in->kode' and status='0' limit 1";
        $res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            $row = $res1->row();
            //$data[] = $row;
            //$s->data = $data;
            $s->status = true;
            $s->penerima = 'armada';
            $s->jenis = $row->jenis_armada;
            $s->nomor = $row->no_armada;
            $s->flag = $row->flag;

            $sql2 = "SELECT jenis_armada, no_armada, check_point FROM detail_armada_pengiriman where kode_pengiriman='$in->kode'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    $data[] = $r;
                }
                $s->data = $data;
            }
        } else {
            $sql2 = "select * from (select kode_pengiriman, nomor_surat from pengantar_barang) ta natural join (select kode_pengiriman, kode_packing from detail_pengiriman group by kode_pengiriman) tb natural join (select IdPengemudi, kode_pengiriman, tgl_pengiriman, status from pengiriman) tc natural join (select idKpuTujuan, kode_packing, kpu_tujuan, kpu_pemesan from packing group by kode_packing) td natural join (select IdMasterKpu as idKpuTujuan, alamat_kantor from master_kpu) te natural join (select IdMasterKpu as idKpuTujuan, nama_pegawai, no_kontak from penerima_logistik) tf natural join (select kode_pengiriman, Flag from serah_terima_kpud) tg where kode_pengiriman='$in->kode'";
            $res2 = $this->db->query($sql2);
            $num2 = $res2->num_rows();
            if($num2>0){
                foreach ($res2->result() as $r){
                    $data[] = $r;
                }
                $s->status = true;
                $s->penerima = 'kpud';
                $s->data = $data;
            }
        }

        return $s;
    }

    function getListFlag($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql = "SELECT * FROM serah_terima where id_pengiriman='$in->id'";
        $res = $this->db->query($sql);
        $num = $res->num_rows();
        if($num>0){
            foreach ($res->result() as $r){
                $data[] = $r;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
    }

    function cekFlag($input){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;

        $sql = "SELECT * FROM serah_terima_kpud where kode_pengiriman='$in->kode' and Status='1'";
        $res = $this->db->query($sql);
        $num = $res->num_rows();
        if($num>0){
            $s->status = true;
        }

        return $s;
    }

    function updateFlag($input, $lokasi){
        $in = a2o($input);
        $s = new stdClass();
        $s->status = false;

        $sql = "UPDATE `serah_terima_kpud` SET latitude='$in->latitude', longitude='$in->longitude', namalokasi='$lokasi' WHERE kode_pengiriman = '$in->kode'";
        $this->db->query($sql);
        $done = ($this->db->affected_rows() != 1) ? false : true;

        if($done){
            $s->status = true;
        }

        return $s;
    }

    function uploadPhoto($seq, $kode, $id){
        $new_image_name = urldecode($_FILES["file"]["name"]);
        $s = new stdClass();
        $s->status = false;
        $res = $this->do_upload($seq);
        if($res){
            if($seq==0) $sql = "UPDATE serah_terima_kpud SET gambar = '$new_image_name' WHERE kode_pengiriman = '$kode' and kode_kpu = '$id'";
            else $sql = "UPDATE serah_terima_kpud SET BeritaAcara = '$new_image_name' WHERE kode_pengiriman = '$kode' and kode_kpu = '$id'";
            $this->db->query($sql);
            $done = ($this->db->affected_rows() != 1) ? false : true;

            if($done){
                $s->status = true;
            }
        }
        return $s;
    }

    public function do_upload($s){
        $_FILES['userfile'] = $_FILES['file'];

        if($s==0) $config['upload_path'] = '/var/www/html/kpuv3/assets/upload/penerima';
        else $config['upload_path'] = '/var/www/html/kpuv3/assets/upload/ba';
        $config['allowed_types']        = '*';
        $config['file_name']            = urldecode($_FILES["file"]["name"]);

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile')){
            $error = array('error' => $this->upload->display_errors());
            return false;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return true;
        }
    }
}
