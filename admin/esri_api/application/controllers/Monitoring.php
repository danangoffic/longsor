<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {
    public function index()
    {
        echo 'Access Denied!';
    }

    private function checkOp($in){
        if(empty($in)) {
            $s = new stdClass();
            $s->status = false;
            $s->message = "Access Denied!";
            printJSON($s);
        }
    }
    
    function getLaporanMonitoring(){
    	$this->load->model('model_monitoring');
    	$list = $this->model_monitoring->laporan_monitoring();
		printJSON($list);
    }


}
