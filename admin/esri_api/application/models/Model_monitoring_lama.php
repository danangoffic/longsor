<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_monitoring extends CI_Model {
    
    function laporan_monitoring()
	{
		
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "select * from (select kode_pengiriman, max(status_pengiriman) as status_pengiriman from status_pengiriman ta group by kode_pengiriman) as td natural JOIN tracking_lokasi natural JOIN (select kode_pengiriman, max(id) as id from tracking_lokasi group by kode_pengiriman) as te natural join pengiriman where status_pengiriman<6";
		$res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            foreach ($res1->result() as $r1){
                
                $data[] = $r1;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
	}
}
