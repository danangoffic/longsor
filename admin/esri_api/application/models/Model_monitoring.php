<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_monitoring extends CI_Model {
    
	
	
    function laporan_monitoring()
	{
		$this->load->helper('fungsi_helper');
		
        $s = new stdClass();
        $s->status = false;
        $data = [];

        $sql1 = "select * from (select kode_pengiriman, max(status_pengiriman) as status_pengiriman 
        		from status_pengiriman ta group by kode_pengiriman) as td 
        		natural join tracking_lokasi natural join (select kode_pengiriman, max(id) as id 
        		from tracking_lokasi group by kode_pengiriman ) as te 
        		join pengiriman on tracking_lokasi.kode_pengiriman = pengiriman.kode_pengiriman where status_pengiriman<6 ";
		$res1 = $this->db->query($sql1);
        $num1 = $res1->num_rows();
        if($num1>0){
            foreach ($res1->result() as $r1){
                
                //$data[] = $r1;
                $row = array();
				$row['Kode Pengiriman'] = $r1->kode_pengiriman;
				//$row[] = $r1->id;
				$row['Satutus Pengiriman'] = get_status_pengiriman($r1->status_pengiriman);
				$row['Tracking Latitude'] = $r1->latitude;
				$row['Tracking Longitude'] = $r1->longitude;
				$row['Tracking Bearing'] = $r1->bearing;
				$row['Tracking Speed'] = $r1->speed;
				$row['Tracking Tgl'] = convert_date(substr($r1->waktu, 0,10));
				$row['Tracking Waktu'] = substr($r1->waktu, 11,17);
				$row['Periode Pemilihan'] = get_nama_periode($r1->kode_periode);
				$row['Kode Vendor'] = $r1->kode_vendor;
				$row['Nama Vendor'] = get_nama_vendor($r1->kode_vendor);
				$row['Nomor Polisi'] = $r1->no_kendaraan;
				$row['Nama Pengemudi'] = get_nama_pengemudi($r1->IdPengemudi);
				$row['Tgl Pengiriman'] = convert_date($r1->tgl_pengiriman);
				$row['Suarat Pengatar'] = $r1->surat_pengantar;
				$row['Bast Pengiriman'] = $r1->bast;
				$row['Flag Pengiriman'] = $r1->flag;
				//$row[] = $r1->status;
				
				$data[] = $row;
            }
            $s->status = true;
            $s->data = $data;
        }

        return $s;
	}
}
