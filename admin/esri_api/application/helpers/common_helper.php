<?php
function a2o($a) {
    return (object) $a;
}

function o2a($o) {
    return (array) $o;
}

function printJSON($v){
    header('Access-Control-Allow-Origin: *');
    header("Content-type: application/json");
    echo json_encode($v);
    exit;
}