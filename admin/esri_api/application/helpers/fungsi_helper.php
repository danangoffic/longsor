<?php
if (! function_exists('convert_date')) {
    function convert_date($date)
    {
        $data = explode('-', $date);
        $year = $data[0];
        $month = $data[1];
        $day = $data[2];

        $m = array('00'=>'00', '01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mai', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember');

        $result = $day.' '.$m[$month].' '.$year;

        return $result;
    }
}

function get_nama_pengemudi($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('IdPengemudi', $id);
	$result = $CI->db->get('pengemudi')->row();
	
	return $result->nama_pengemudi;
	
}

function get_status_pengiriman($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('IdStatus', $id);
	$result = $CI->db->get('status')->row();
	
	return $result->status;
	
}

function get_kode_pengiriman_via_kdpemesanan($id=false) {
    
	$CI = & get_instance();
	$query_b = $CI->db->query("SELECT * FROM `packing` as p, detail_pengiriman as d where p.kode_packing=d.kode_packing and p.kode_pemesanan='$id' group by d.kode_pengiriman  ");
	$row_b = $query_b->row();
	
	return $row_b->kode_pengiriman;
	
}

function get_nama_vendor($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('kode_perusahaan', $id);
	$result = $CI->db->get('data_vendor')->row();
	
	return $result->nama_perusahaan;
	
}

function get_kode_periode() {
    
	$CI = & get_instance();
	$CI->db->where('status_aktif', '1');
	$result = $CI->db->get('periode_pemilihan')->row();
	
	return $result->kode_periode;
	
}

function get_nama_kpu($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('IdMasterKpu', $id);
	$result = $CI->db->get('master_kpu')->row();
	
	return $result->nama_satker;
	
}

function get_nama_periode($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('kode_periode', $id);
	$result = $CI->db->get('periode_pemilihan')->row();
	
	return $result->jenis_periode;
	
}

function get_wilayah_kpu($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('IdMasterKpu', $id);
	$result = $CI->db->get('master_kpu')->row();
	
	return $result->wilayah;
	
}

function get_alamat_kpu($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('IdMasterKpu', $id);
	$result = $CI->db->get('master_kpu')->row();
	
	return $result->alamat_kantor;
	
}

function get_provinsi_kpu($id=false) {
	$CI = & get_instance();
	$query_b = $CI->db->query("SELECT * FROM `master_kpu` where IdMasterKpu='$id' ");
	$row_b = $query_b->row();
	$kode_prov= $row_b->kode_provinsi;
    
	
	$CI->db->where('kode_provinsi', $kode_prov);
	$result = $CI->db->get('master_provinsi')->row();
	
	return $result->nama_provinsi	;
	
}

function get_nama_barang($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('kode_barang', $id);
	$result = $CI->db->get('jenis_barang_logistik')->row();
	
	return $result->nama_barang;
	
}

function get_satuan_barang($id=false) {
    
	$CI = & get_instance();
	$CI->db->where('kode_barang', $id);
	$result = $CI->db->get('jenis_barang_logistik')->row();
	
	return $result->satuan;
	
}
?>