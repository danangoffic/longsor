<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?= base_url() ?>assets/images/logo/logo.png" type="image/x-icon">
        <link rel="icon" href="<?= base_url() ?>assets/images/logo/logo.png" type="image/x-icon">

        <title>Aplikasi Monitoring Longsor</title>
        <!--Bootstrap Core CSS--> 
        <link href="<?= base_url() ?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bower_components/icheck/skins/all.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <!--Menu CSS--> 
        <link href="<?= base_url() ?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!--toast CSS--> 
        <link href="<?= base_url() ?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
        <!--animation CSS--> 
        <link href="<?= base_url() ?>assets/css/animate.css" rel="stylesheet">
        <!--Custom CSS--> 
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?= base_url('assets/css/jquery.autocomplete.css') ?>" />
        <!--<link href="<?= base_url() ?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />-->
        <!--color CSS--> 
        <link href="<?= base_url() ?>assets/css/colors/blue-dark.css" id="theme" rel="stylesheet">
        <!--alerts CSS--> 
        <link href="<?php echo base_url() ?>assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

        <link href="<?php echo base_url() ?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <style>
            .loader {
              border: 8px solid #f3f3f3;
              border-radius: 50%;
              border-top: 8px solid #3498db;
              width: 80px;
              height: 80px;
              -webkit-animation: spin 1s linear infinite; /* Safari */
              animation: spin 1s linear infinite;
            }

            /* Safari */
            @-webkit-keyframes spin {
              0% { -webkit-transform: rotate(0deg); }
              100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
              0% { transform: rotate(0deg); }
              100% { transform: rotate(360deg); }
            }
        </style>


        <?=isset($head_extend) ? $this->load->view($head_extend) : '';?>
    </head>

    <body>
        <!-- Preloader -->
<!--        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>-->
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header"> 
                    <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>

                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                                <img src="<?= base_url()?>assets/img/user/user.png" class="online" width="30px"/>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="<?= base_url('user/edit/' . $this->session->userdata('id_user')); ?>" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?= base_url('user/logout'); ?>" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                                </li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
                    </ul>
                    <div class="heading-title pull-right" style="margin-top: 10px">
                        <h5><b><?= $this->session->userdata('perusahaan'); ?></b></h5>
                    </div>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <!-- Left navbar-header -->
            <?php echo $this->load->view('navigation'); ?>
            <!-- Left navbar-header end -->
            <!-- Page Content -->
            <div id="page-wrapper">
                <?php echo isset($view) ? $this->load->view($view) : ''; ?>
                <footer class="footer text-center">
                    <span class="txt-color-white"><span class="hidden-xs">Aplikasi Monitoring Longsor</span> - 2018</span>
                </footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <?php echo $this->load->view('script_general'); ?>
        <?php echo isset($script_spesific) ? $this->load->view($script_spesific) : ''; ?>

    </body>

</html>
