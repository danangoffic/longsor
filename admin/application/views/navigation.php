<?php
?>
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu" style="font-size: 12px;">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">

            </li>
            <?php $akses = $this->session->userdata('access'); ?>

            
                <li class="waves-effect <?= ($modul == 'dashboard') ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() ?>" ><i class="fa fa-lg fa-home fa-fw"></i> <span class="hide-menu">Dashboard</span></span></a>
                </li>
                <li class="waves-effect <?= ($modul == 'lokasi') ? 'active' : ''; ?>">
                    <a href="<?php echo base_url('lokasi') ?>" ><i class="fa fa-lg fa-fw fa-map-marker"></i> <span class="hide-menu">Lokasi</span></span></a>
                </li>
                <li>
                    <a href="<?= base_url('alat'); ?>" title="Alat" class="waves-effect <?= ($modul == 'alat') ? 'active' : ''; ?>"><i class="fa fa-lg fa-fw fa-archive"></i> <span class="hide-menu" >Alat</span></a>
                </li>
                <li class="waves-effect <?= ($modul == 'monitoring') ? 'active' : ''; ?>">
                    <a href="<?= base_url('monitoring'); ?>" title="Monitoring"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">Monitoring</span></a>
                </li>
                <li>
                    <a class="waves-effect <?= ($modul == 'laporan') ? 'active' : ''; ?>" href="<?=base_url('laporan')?>" title="Laporan"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Laporan</span></a>
                </li>
                <li>
                    <a class="waves-effect <?= ($modul == 'user') ? 'active' : ''; ?>" href="#" title="User"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">User</span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?= base_url('user'); ?>" title="Data User">Data User</a>
                        </li>
                        <li>
                            <a href="<?= base_url('user/add'); ?>" title="Tambah Data User">Tambah Data User</a>
                        </li>
                    </ul>
                </li>  
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->