<?php
/**
 * 
 */
class Channel
{
	private $host = 'locahost', $port=8080, $socket;

	public function connect()
	{
		if (($this->socket=socket_create(AF_INET, SOCKET_STREAM, SOL_TCP))) {
			return socket_connect($this->socket, $this->host, $this->port);
		}else{
			return false;
		}
	}

	public function send($buffer)
	{
		try {
			if ($this->connect()) {
				if (socket_write($this->socket, $buffer, strlen($buffer))) {
					socket_close($this->socket);
					return true;
				}
			}else{
				return false;
			}
		} catch (Exception $e) {
			
		}
	}
}