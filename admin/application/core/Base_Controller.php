<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* load the MX_Loader class */
// require APPPATH."third_party/MX/Controller.php";
abstract class Base_controller extends MX_Controller
{

	/**
     * property layout untuk template
     * @var string
     */
	protected $layout;

	public function __construct()
	{
        parent::__construct();
        $this->form_validation->CI =& $this;
		$this->layout = 'layout';
	}

	protected function view($data)
	{
		$this->load->view($this->layout, $data);
	}

	public function paginate(array $data)
    {
        $trace = debug_backtrace();
        $base_url = (! array_key_exists('base_url', $data))
            ? base_url(strtolower(get_class($this)).'/'.strtolower($trace[1]["function"]).'/')
            : $data['base_url'];

        $per_page = (! array_key_exists('per_page', $data))
            ? 20
            : $data['per_page'] ;

        $total = (! array_key_exists('total_rows', $data))
            ? 200
            : $data['total_rows'] ;

        $segment = (! array_key_exists('segment', $data))
            ? 3
            : $data['segment'] ;

        return $this->create_pagination($base_url, $per_page, $total, $segment);
    }

	protected function create_pagination($base_url, $per_page, $total, $segment)
    {
        $config['full_tag_open']  = '<ul class="pagination pagination-sm pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['next_tag_open']      = '<li>';
        $config['next_tag_close']      = '</li>';
        $config['prev_tag_open']      = '<li>';
        $config['prev_tag_close']      = '</li>';
        $config['num_tag_open']   = '<li>';
        $config['num_tag_close']  = '</li>';
        $config['cur_tag_open']   = '<li class="active"><a>';
        $config['cur_tag_close']  = '</a></li>';
        $config['num_links']      = 10;
        $config['last_link']      = '<li><a></a></li>';
        $config['first_link']     = '<li><a></a></li>';
        $config['base_url']       = $base_url;
        $config['per_page']       = $per_page;
        $config['total_rows']     = $total;
        $config['uri_segment']    = $segment;
        $config['use_page_numbers'] = TRUE;

        $this->load->library('pagination');
       return $this->pagination->initialize($config);

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */