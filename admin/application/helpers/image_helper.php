<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');

if (! function_exists('image')) {
    function image($image_path, $preset)
    {
        $CI = &get_instance();

        // load the allowed image presets
        $CI->load->config("images");
        // return var_dump($CI->load->config("images"));
        $sizes = $CI->config->item("image_sizes");

        $pathinfo = pathinfo($image_path);
        $new_path = $image_path;

        // check if requested preset exists
        if (isset($sizes[$preset])) {
            // $new_path = $pathinfo["dirname"] . "/" . $pathinfo["filename"] . "-" . $preset . "." . $pathinfo["extension"];
            $new_path = $pathinfo["dirname"] . "/" . $pathinfo["filename"] . "-" . implode("x", $sizes[$preset]) . "." . $pathinfo["extension"];
        }

        return $new_path;
    }
}

if (! function_exists('image_upload')) {
    function image_upload($path, $image_name)
    {
        $CI = &get_instance();
        $config = array(
            'upload_path'   => 'assets/upload/'.$path,
            'allowed_types' => 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG',
            'max_size'      => 5000,
            'encrypt_name'  => true,
        );

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        if(! $CI->upload->do_upload($image_name)) {
            $CI->session->set_flashdata('error', $sub_data['error'] = $CI->upload->display_errors());
            return false;
        }

        $sub_data = $CI->upload->data();
        foreach ($sub_data as $item => $data) {
            $items = $sub_data['file_name'];
        }

        return $items;
    }
}

if (! function_exists('do_resize')) {
    function do_resize($path, $image_name) {
        $CI = &get_instance();
        $source_path = 'assets/upload/'.$path.'/'.$image_name;
        $target_path = 'assets/upload/'.$path.'/thumbnail';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 100,
            'height' => 100
        );
        
        $CI->load->library('image_lib', $config_manip);
        $CI->image_lib->initialize($config_manip);
        if (!$CI->image_lib->resize()) {
            echo $CI->image_lib->display_errors();
            
        } else {
            return $image_name;
        }
        // clear //
        $CI->image_lib->clear();
    }
}

if (! function_exists('image_remove')) {
    function image_remove($path, $image_name)
    {


        $pathinfo = pathinfo('assets/upload/'.$path.'/'.$image_name);
        $dirname  = $pathinfo['dirname'];
        $filename = $pathinfo['filename'];

        $files = glob($dirname.'/'.$filename.'*');
        array_walk($files, function ($files) {
            if (file_exists($files)) {
                unlink($files);
            }
        });

        /*foreach (glob($dirname.'/'.$filename.'*') as $filelist) {
            if (file_exists($filelist)) {
               @unlink($filelist);
            }
        } */
    }
}