<?php

if (!function_exists('redirect_back')) {

  function redirect_back() {
    $CI = &get_instance();
    redirect($CI->input->server('HTTP_REFERER', TRUE), 'location');
  }

}

if (!function_exists('convert_date')) {

  function convert_date($date) {
    $data = explode('-', $date);
    $year = $data[0];
    $month = $data[1];
    $day = $data[2];

    $m = array('00' => '00', '01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mai', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');

    $result = $day . ' ' . $m[$month] . ' ' . $year;

    return $result;
  }

}

if (!function_exists('file_icon')) {

  function file_icon($file, $format) {
    if ($format == 'img') {
      $result = '<img class="img-responsive" src="' . base_url('assets/uploads/media/' . $file) . '">';
    } elseif ($format == 'doc') {
      $result = '<img class="img-responsive" src="' . base_url('assets/images/doc.png') . '">';
    } elseif ($format == 'xls') {
      $result = '<img class="img-responsive" src="' . base_url('assets/images/xls.png') . '">';
    } elseif ($format == 'ppt') {
      $result = '<img class="img-responsive" src="' . base_url('assets/images/ppt.png') . '">';
    } elseif ($format == 'pdf') {
      $result = '<img class="img-responsive" src="' . base_url('assets/images/pdf.png') . '">';
    } elseif ($format == 'archive') {
      $result = '<img class="img-responsive" src="' . base_url('assets/images/zip.png') . '">';
    } else {
      $result = '<img class="img-responsive" src="' . base_url('assets/images/unknown.png') . '">';
    }

    return $result;
  }

}


if (!function_exists('link_post')) {

  function link_post($content, $id, $title) {
    $result = base_url($content . '/' . $id . '/' . url_title(strtolower($title)));

    return $result;
  }

}

if (!function_exists('rupiah')) {

  function rupiah($value, $step = 0) {
    $rupiah = number_format($value, $step, ',', '.') . "";
    return $rupiah;
  }

}