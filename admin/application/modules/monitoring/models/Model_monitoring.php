<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_monitoring extends CI_Model
{

	public function hujan_by_lokasi($id_lokasi)
	{
		$this->db->select('h.id_alat, h.curah_hujan, h.tanggal, h.jam, a.nama_alat, l.id_lokasi, dayofyear(h.tanggal) as tgl');
		$this->db->from('data_hujan h');
		$this->db->join('data_alat a', 'h.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi', $id_lokasi);
		$this->db->order_by('h.nomor', 'DESC');
		$this->db->limit(1);
		return $this->db->get();
	}

	public function getaran_by_lokasi($id_lokasi)
	{
		$this->db->select('g.id_alat, g.frequensi, g.jam, a.nama_alat, l.id_lokasi');
		$this->db->from('data_getaran g');
		$this->db->join('data_alat a', 'g.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi', $id_lokasi);
		$this->db->order_by('g.nomor', 'DESC');
		return $this->db->get();
	}

	public function kelembaban_by_lokasi($id_lokasi)
	{
		$this->db->select('k.id_alat, k.kelembaban_tanah, k.jam, a.nama_alat, l.id_lokasi');
		$this->db->from('data_kel_tanah k');
		$this->db->join('data_alat a', 'k.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi', $id_lokasi);
		$this->db->order_by('k.nomor', 'DESC');
		$this->db->limit(3);
		return $this->db->get();
	}

	public function suhu_by_lokasi($id_lokasi)
	{
		$this->db->select('s.id_alat, s.suhu_tanah, s.jam, a.nama_alat, l.id_lokasi');
		$this->db->from('data_su_tanah s');
		$this->db->join('data_alat a', 's.id_alat = a.id_alat');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi', $id_lokasi);
		$this->db->order_by('s.nomor', 'DESC');
		$this->db->limit(3);
		return $this->db->get();
		
	}
}