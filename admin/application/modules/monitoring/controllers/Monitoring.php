<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends Base_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('lokasi/Model_lokasi', 'lokasi');
    $this->load->model('Model_monitoring', 'monitoring');
    if (!$this->session->userdata('username')) {
      redirect('user/login');
    }
  }

  public function index() {
  	if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $page_variable = array(
            'title'     => 'Monitoring',
            'modul'  => 'monitoring',
            'lokasi' => $this->lokasi->all(),
            'view'      =>'index',
            'head_extend' => 'head_index',
            'script_spesific' => 'script_index'
        );

        return $this->view($page_variable);
  }

  public function data_lokasi($id_lokasi)
  {
    $page_variable = array(
            'title'     => 'Monitoring',
            'modul'  => 'monitoring',
            'lokasi' => $this->lokasi->by_id($id_lokasi),
            'hujan' => $this->monitoring->hujan_by_lokasi($id_lokasi),
            'getaran' => $this->monitoring->getaran_by_lokasi($id_lokasi),
            'kelembaban' => $this->monitoring->kelembaban_by_lokasi($id_lokasi),
            'suhu' => $this->monitoring->suhu_by_lokasi($id_lokasi),
            'view'      =>'iframe'
        );
    $this->load->view('iframe', $page_variable);
  }


}
