<!-- CONTAINER CONTENT -->
<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title"><?=ucfirst($title);?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        <li><a href="<?=site_url('admin');?>">Dashboard</a></li>
        <li class="active"><?=$title?></li>
      </ol>
    </div>

  </div>

  <div class="row">
    <div class="col-sm-12">
      <p class="text-muted m-b-5"></p>
      <!-- start of alert html -->
      <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-check"></i>
      <strong>Success</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif ?>
      <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-times"></i>
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php endif ?>
      <!-- end of alert html -->
    </div>
	  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        <div class="panel panel-info">
	            <div class="panel-heading"><?=$title;?></div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                	<div id="mapp" style="height: 450px; width: 100%;"></div>
	                </div>
	            </div>
	      	</div>
	  	</div>
  </div>
</div>
</div>
<!-- END CONTAINER CONTENT -->
