<html><head>
                <meta charset="utf-8&quot;">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <!-- Latest compiled and minified CSS -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
                
                <!-- Optional theme -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
                
                <!-- Latest compiled and minified JavaScript -->
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

                <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
                <title>Monitoring Longsor</title>
            </head>
    
            <body style="font-family: 'Roboto', sans-serif;">
                <div style="position: fixed;background-color:#F6D86B;color:000000; text-align:center; padding:2px 5px 2px 5px; width:100%; height:22px">
                    <p style="font-size:12px"><b><?=$lokasi->row()->lokasi;?></b></p>
                </div>
                <div style="background:#ffffff; top:22px; bottom:14px; text-align:justify; font-size:12px; padding:24px 2px 2px 2px; width:100%;">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Lokasi</strong><br>
                            Longitude: <?=$lokasi->row()->lon;?><br>
                            Latitude: <?=$lokasi->row()->lat;?>
                        </li>
                        <li class="list-group-item">
                            <strong>Waktu Pengamatan</strong><br>
                            <?=$hujan->row()->tanggal;?> 00:00:00-<?=$hujan->row()->jam;?> WIB
                        </li>
                        <li class="list-group-item">
                            <strong class="text-center">Kondisi:</strong><br>
                            <strong>Suhu:</strong>
                            <?php
                            $n=1;
                            if($suhu->num_rows() > 0):
                                foreach ($suhu->result() as $rows) {
                                    echo $rows->suhu_tanah . "&deg;C";
                                    if($n==1||$n==2){
                                        echo " | ";
                                    }
                                    $n++;
                                }
                            endif;
                            ?><br>
                            <strong>Kelembaban:</strong>
                            <?php 
                            $n=1;
                            if($kelembaban->num_rows() > 0):
                                foreach ($kelembaban->result() as $rowk) {
                                    echo $rowk->kelembaban_tanah . "%";
                                    if($n==1||$n==2){
                                        echo " | ";
                                    }
                                    $n++;
                                }
                            endif;
                             ?><br>
                            <strong>Curah Hujan:</strong>
                            <?php
                            if($hujan->num_rows() !== 0):
                                echo $hujan->row()->curah_hujan . " mm";
                            endif;
                            ?><br>
                            <strong>Getaran:</strong>
                            <?php
                            if($getaran->num_rows() !== 0):
                                echo $getaran->row()->frequensi . " hZ";
                            endif;
                            ?>
                        </li>
                        <li class="list-group-item">
                            <strong>Last Issued:</strong><br>
                            <?=str_replace("-", "", $hujan->row()->tanggal);?>/<?=$lokasi->row()->id_lokasi;?>
                        </li>
                    </ul>
                </div>
                    <!-- <dl> -->
                        
                        <!-- <hr style="margin:0px 0px 0px 0px">
                        <dt>Pengamatan Kegempaan:</dt>
                        <dd><img src="https://magma.vsi.esdm.go.id/img/eqhist/SAN.png" alt="Kegempaan Sangeangapi" width="100%"></dd>
                        <hr style="margin:0px 0px 0px 0px">
                        <dt>Kesimpulan:</dt>
                        <dd>Tingkat Aktivitas G. Sangeangapi Level II (Waspada)</dd>
                        <hr style="margin:0px 0px 0px 0px">
                        
                        <hr style="margin:0px 0px 0px 0px">
                        <dt>Volcano Observatory Notice for Aviation (VONA):</dt>
                        <dd><u>Last Issued:</u><br>
                            20181015/0813Z<br>
                            <u>Current Aviation Color Code:</u><br>
                            ORANGE<br>
                            <u>Volcanic Activity Summary:</u><br>
                            Ash Emission with volcanic ash cloud at 0538 UTC (1338 local) and is continuing<br>
                            <u>Volcanic Cloud Height:</u><br>
                            Best estimate of ash-cloud top is around 7037 FT (2199 M) above sea level, may be higher than what can be observed clearly. Source of height data: ground observer.<br>
                            <u>Other Volcanic Cloud Information:</u><br>
                            Ash-cloud moving to west - northwest-southwest<br>
                            <u>Remarks:</u><br>
                            Seismic activity is dominated with gas emissions and local tectonic earthquake<br>
                            <a href="https://magma.vsi.esdm.go.id/vona/display.php?noticenumber=2018SAN03" target="_blank">VONA Detail</a> :: <a href="https://magma.vsi.esdm.go.id/vona" target="_blank">VONA Archive</a>
                        </dd>
                        <hr style="margin:0px 0px 0px 0px">
                        <dt>Pelapor:</dt>
                        <dd>Hadi Purwoko, A.Md., </dd>
                        <hr style="margin:0px 0px 0px 0px">
                        <dt>Sumber data:</dt>
                        <dd>Kementerian ESDM, Badan Geologi, PVMBG<br>Pos Pengamatan Gunung Api Sangeangapi</dd>
                        <hr style="margin:0px 0px 0px 0px">
                        <dt>Download/Bagikan:</dt>
                            <div align="center" style="width: 30px; padding: 5px; border: 0; box-shadow: 0; display: inline; text-align: center">
                                <a download="MAGMA-VAR-Sangeangapi-2018-11-12.png" href="https://magma.vsi.esdm.go.id/img/varshare/SAN.jpg" title="MAGMA-VAR-Sangeangapi-2018-11-12">
                                    <img style="border:0px;margin:0px 5px 5px 0px;padding: 0px 10px 5px 0px;float:left;width:27px;" src="https://magma.vsi.esdm.go.id/img/icon/ic_download.png">
                                </a>
                                <a href="http://www.facebook.com/sharer.php?u=https://magma.vsi.esdm.go.id/img/varshare/SAN.jpg" target="_blank">
                                    <img style="border:0px;margin:0px 5px 5px;padding: 0px 10px 5px 0px;float:left;width:27px;" src="https://magma.vsi.esdm.go.id/img/icon/ic_facebook.png" alt="Share to Facebook">
                                </a>
                                <a href="https://twitter.com/share?url=https://magma.vsi.esdm.go.id/img/varshare/SAN.jpg" target="_blank">
                                    <img style="border:0px;margin:0px 5px 5px;padding: 0px 10px 5px 0px;float:left;width:27px;" src="https://magma.vsi.esdm.go.id/img/icon/ic_twitter.png" alt="Share to Twitter">
                                </a>
                                
                                <a data-action="share/whatsapp/share" href="whatsapp://send?text=MAGMA Indonesia | Volcanic Activity Report (VAR) G. Sangeangapi periode 2018-11-12 https://magma.vsi.esdm.go.id/img/varshare/SAN.jpg">
                                    <img style="border:0px;margin:0px 5px 5px;padding: 0px 10px 5px 0px;float:left;width:27px;" src="https://magma.vsi.esdm.go.id/img/icon/ic_whatsapp.png" alt="Share to Whatsapp">
                                </a>
                            </div>
                    </dl><div style="position: fixed;background:#F6D86B;color:000000; text-align:center; left:0; bottom:0; width:100%; height:14px">
                    <p style="font-size:9px">Magma Indonesia © 2015 KESDM-BG-PVMBG</p>
                </div><table style="width:100%"> -->
                    </body></html>