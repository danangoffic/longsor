
<script type="text/javascript">
        var Layers = L.layerGroup();
        
        <?php
        $n=1;
        foreach ($lokasi->result() as $rowl) {
            ?>
            var popup_ini<?=$n;?> = L.popup({maxWidth: '295'});
            var frame = '<iframe src="<?=base_url('monitoring/data_lokasi/').$rowl->id_lokasi;?>" width="280" height="280"></iframe>';
            popup_ini<?=$n;?>.setContent(frame);
            <?php
            if($n>1){
                echo "\t\t";
            }
            echo "L.marker([".$rowl->lat.", ".$rowl->lon."]).bindPopup(popup_ini".$n.").addTo(Layers);\n";
            $n++;
        }
        ?>

        var token_map = 'pk.eyJ1IjoiZGFuYW5nb2ZmaWMiLCJhIjoiY2o3NjlkODFvMHlpeDMzczNhMTVjZnE0eCJ9.iW12sX94_cMv6P0ivbqbSA';

        //attribution
                attr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
          '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
          'Imagery © <a href="http://mapbox.com">Mapbox</a>';

        //var url base layer
                var url = 'https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token='+token_map,
                    url2 = 'https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token='+token_map;
        var sattelite = L.tileLayer(url, {id: 'mapbox.sattelite', attribution: attr}),
                    streets  = L.tileLayer(url2, {id: 'mapbox.streets',   attribution: attr});
        var mymap = L.map('mapp', {
                    center: [-7.7858,110.3755],
                    zoom: 11,
                    layers: [streets, Layers]
                });
        //var untuk load map tilelayer
        var load_map = L.tileLayer(url2, {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18,
            minZoom: 8,
            id: 'mapbox.streets',
            accessToken: token_map
        }).addTo(mymap);

        //=========LAYERING==========//
        
        var baseLayers = {
            "Streets": streets,
                "Sattelite": sattelite
            };
        
        //var overlays
        var overlays = {
            "Layers": Layers
        };
        
        //control layer berisi baselayer dan overlays
        L.control.layers(baseLayers, overlays).addTo(mymap);
                
                
    //var popup lat long onclick
        var popup_latlng = L.popup();
            function onMapClick(e) {
                popup_latlng
                    .setLatLng(e.latlng)
                    .setContent(
                    "Latitude: " + e.latlng.lat.toFixed(4)+
                    "<br>Longitude: " + e.latlng.lng.toFixed(4)
                        )
                    .openOn(mymap);
            }
            
            //action popup lat long onclick
            mymap.on('click', onMapClick);
        
    </script>