<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Base_Controller {

  public function __construct() {
    parent::__construct();
    

    if (!$this->session->userdata('username')) {
      redirect('user/login');
    }
  }

  public function index() {
  	if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $page_variable = array(
            'title'     => 'dashboard',
            'modul'  => 'dashboard',
            'view'      =>'index'
        );

        return $this->view($page_variable);
  }
}
