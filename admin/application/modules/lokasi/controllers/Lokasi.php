<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends Base_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('Model_lokasi', 'lokasi');

    if (!$this->session->userdata('username')) {
      redirect('user/login');
    }
  }

  public function index() {
  	if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $page_variable = array(
            'lokasi' => $this->lokasi->all(),
            'title' => 'Lokasi',
            'subtitle' => 'Data Lokasi',
            'modul' => 'lokasi',
            'view' =>'index',
            'script_spesific'=>'script_index'
        );

        return $this->view($page_variable);
  }

  public function get_alat($id_lokasi)
  {
    $page_variable = array(
      'all_alat' => $this->lokasi->all_alat_by_lokasi($id_lokasi),
      );
    return $this->load->view('alat_lokasi', $page_variable);
  }

  public function add()
  {
    if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $page_variable = array(
            'title' => 'Lokasi',
            'subtitle' => 'Add Lokasi',
            'modul' => 'lokasi',
            'view' =>'add',
            'head_extend' => 'head_add',
            'script_spesific' => 'script_add'
        );

    return $this->view($page_variable);
  }

  public function edit($id)
  {
    $page_variable = array(
            'lokasi' => $this->lokasi->by_id($id)->row(),
            'title' => 'Lokasi',
            'subtitle' => 'Edit Lokasi ' . $this->lokasi->by_id($id)->row()->lokasi,
            'modul' => 'lokasi',
            'view' =>'edit',
            'head_extend' => 'head_edit',
            'script_spesific' => 'script_edit'
        );

    return $this->view($page_variable);
  }

  public function save()
  {
    $this->form_validation->set_rules('id_lokasi', 'ID Lokasi', 'required');
    $this->form_validation->set_rules('lokasi', 'Lokasi', 'required');

    if (!$this->form_validation->run()) {
        $this->session->set_flashdata('error', validation_errors());
        redirect_back();
    }
    $data = array(
        'id_lokasi' => $this->input->post('id_lokasi'),
        'lokasi' => $this->input->post('lokasi'),
        'lat' => $this->input->post('lat'),
        'lon' => $this->input->post('lon')
      );
    $insert = $this->lokasi->insert($data);
    if($insert){
      $this->session->set_flashdata('success', 'Data Lokasi Berhasil Disimpan');
      redirect('lokasi');
    }else{
      $this->session->set_flashdata('error', 'Gagal Menyimpan Data Lokasi');
      redirect_back();
    }
  }

  public function update()
  {
    $this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
    $id = $this->input->post('id');
    if (!$this->form_validation->run()) {
        $this->session->set_flashdata('error', validation_errors());
        redirect_back();
    }
    $data = array(
        'lokasi' => $this->input->post('lokasi'),
        'lat' => $this->input->post('lat'),
        'lon' => $this->input->post('lon')
      );
    $update = $this->lokasi->update($id,$data);
    if($update){
      $this->session->set_flashdata('success', 'Data Lokasi Berhasil Disimpan');
      redirect('lokasi');
    }else{
      $this->session->set_flashdata('error', 'Gagal Menyimpan Data Lokasi');
      redirect('lokasi/edit/'.$id);
    }
  }

  public function delete($id)
  {
    if (!isset($id)) {
      redirect('lokasi');
    }
    $delete = $this->lokasi->delete($id);
    if($delete){
      $this->session->set_flashdata('success', 'Data Lokasi Berhasil Dihapus');
      redirect('lokasi');
    }else{
      $this->session->set_flashdata('error', 'Gagal Menghapus Data Lokasi');
      redirect('lokasi');
    }
  }
}
