<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_lokasi extends CI_Model
{
	public function all()
	{
		return $this->db->get('data_lokasi');
	}

	public function insert($data)
	{
		return $this->db->insert('data_lokasi', $data);
	}

	public function by_id($id)
	{
		$this->db->where('id_lokasi',$id);
		return $this->db->get('data_lokasi');
	}

	public function delete($id)
	{
		$this->db->where('id_lokasi', $id);
		return $this->db->delete('data_lokasi');
	}

	public function update($id, $data)
	{
		$this->db->where('id_lokasi', $id);
		return $this->db->update('data_lokasi', $data);
	}

	public function all_alat_by_lokasi($id_lokasi)
	{
		$this->db->select('a.id_alat, a.id_lokasi, a.nama_alat, a.lat, a.lon, l.lokasi');
		$this->db->from('data_alat a');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('a.id_lokasi', $id_lokasi);
		return $this->db->get();
	}

	public function last_hujan($id_lokasi=null)
	{
		if($id_lokasi!==null):
			$this->db->select('h.id_alat, h.tanggal, h.jam, h.curah_hujan, a.id_lokasi');
			$this->db->from('data_hujan h');
			$this->db->where('a.id_lokasi', $id_lokasi);
			$this->db->join('data_alat a', 'h.id_alat = a.id_alat');
			$this->db->order_by('h.nomor', 'DESC');
			$this->db->limit(1);
			return $this->db->get();
		else:
			return 0;
		endif;
	}

	public function last_getaran($id_lokasi=null)
	{
		if($id_lokasi!==null):
			$this->db->select('g.id_alat, g.tanggal, g.jam, g.frequensi, a.id_lokasi');
			$this->db->from('data_getaran g');
			$this->db->where('a.id_lokasi', $id_lokasi);
			$this->db->join('data_alat a', 'g.id_alat = a.id_alat');
			$this->db->order_by('g.nomor', 'DESC');
			$this->db->limit(1);
			return $this->db->get();
		else:
			return 0;
		endif;
	}

	public function last_suhu($id_lokasi = null)
	{
		if($id_lokasi!==null):
			$this->db->select('s.id_alat, s.tanggal, s.jam, s.suhu_tanah as suhu, a.id_lokasi');
			$this->db->from('data_su_tanah s');
			$this->db->where('a.id_lokasi', $id_lokasi);
			$this->db->join('data_alat a', 's.id_alat = a.id_alat');
			$this->db->order_by('s.nomor', 'DESC');
			$this->db->limit(3);
			return $this->db->get();
		else:
			return 0;
		endif;
	}

	public function last_kelembaban($id_lokasi = null)
	{
		if($id_lokasi!==null):
			$this->db->select('k.id_alat, k.tanggal, k.jam, k.kelembaban_tanah as kelembaban, a.id_alat');
			$this->db->from('data_kel_tanah k');
			$this->db->where('a.id_lokasi', $id_lokasi);
			$this->db->join('data_alat a', 'k.id_alat = a.id_alat');
			$this->db->order_by('k.nomor', 'DESC');
			$this->db->limit(3);
			return $this->db->get();
		else:
			return 0;
		endif;
	}
}