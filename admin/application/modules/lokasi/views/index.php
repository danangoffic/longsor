<!-- CONTAINER CONTENT -->
<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title"><?=$subtitle;?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <a class="btn btn-rounded btn-info btn-outline pull-right m-l-5" href="<?=base_url('lokasi/add')?>">Tambah <?=$title?></a>
      <ol class="breadcrumb">
        <li><a href="<?=site_url();?>">Dashboard</a></li>
        <li><a href="#"><?=$title;?></a></li>
        <li class="active"><?=$subtitle?></li>
      </ol>
    </div>

  </div>

  <div class="row">
    <div class="col-lg-12">
      <p class="text-muted m-b-30"></p>
      <!-- start of alert html -->
      
      <!-- end of alert html -->
		  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <?php if ($this->session->flashdata('success')): ?>
          <div class="alert alert-success fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> <?php echo $this->session->flashdata('success'); ?>
          </div>
          <?php endif ?>
          <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-times"></i>
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
          </div>
          <?php endif ?>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading"><?=$subtitle;?></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <div class="row">
                    <div class="table-responsive">
	                	<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
      								<thead>
      									<tr>
      										<th class="text-center">No</th>
      										<th class="text-center">Lokasi</th>
      										<th class="text-center">Aksi</th>
      									</tr>
      								</thead>
      								<tbody>
      									<?php if($lokasi->num_rows() !== 0) {
      										$num = 1;
                          foreach ($lokasi->result() as $row) : ?>
      										<tr>
      											<td class="text-center"><?= $num; ?></td>
      											<td><?= $row->lokasi;?>&nbsp;<span class="text-info" title="ID Lokasi: <?=$row->id_lokasi;?>">(<?=$row->id_lokasi;?>)</span></td>
      											<td nowrap="" class="text-center">
                              <button type="button" class="btn btn-info btn-circle btn-sm text-white" data-toggle="modal" data-target="#modal_detail" data-id="<?=$row->id_lokasi;?>">
                                <i class="ti-search"></i>
                              </button>
      												<a href="<?= base_url('lokasi/edit/'.$row->id_lokasi); ?>" class="btn btn-sm btn-info btn-circle text-white" data-toggle="tooltip" data-title="Edit &quot;<?=$row->lokasi;?>&quot;"><i class="fa fa-pencil"></i></a>
    													<a href="<?= base_url('lokasi/delete/'.$row->id_lokasi); ?>" data-toggle="tooltip" data-title="Delete &quot;<?=$row->lokasi;?>&quot;" class="btn btn-sm btn-danger btn-circle text-white" onclick="return confirm('Are you sure you want to delete this data?')"><i class="fa fa-trash"></i></a>
      											</td>
      										</tr>
      										<?php $num++; endforeach;
      									} ?>
      								</tbody>
      							</table>
                    </div>
                  </div>
                </div>
            </div>
      	</div>
        
      
      
        </div>
    </div>
</div>
</div>
<!-- END CONTAINER CONTENT -->
<div id="modal_detail" class="modal fade" role="dialog">
  <div class="modal-dialog" id="content_modal">
    
  </div>
</div>
