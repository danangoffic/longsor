<script type="text/javascript">
	$("#modal_detail").on('show.modal.bs', function(e){
		var id = $(e.relatedTarget).data('id');
		$("#content_modal").html('');
		$("#content_modal").addClass('loader');
		$.ajax({url: "<?=base_url('lokasi/get_alat/')?>"+id, success: function(result){
			$("#content_modal").removeClass('loader');
	        $("#content_modal").html(result);
		    }, error: function(error){
		    	$("#content_modal").html(error);
		    }});
	})
</script>