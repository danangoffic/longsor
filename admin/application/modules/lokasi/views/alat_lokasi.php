

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>

    <h4 class="modal-title"><?=$all_alat->row()->lokasi;?></h4>
      
  </div>
  <div class="modal-body">
    <a class="btn btn-success btn-rounded btn-outline btn-sm m-b-10 pull-right" href="<?=base_url('alat/add')?>">
      <i class="ti-plus"></i>
      Tambah Alat
    </a>
    <div class="clearfix"></div>
    <div class="table-responsive">
      <table class="table table-condensed table-bordered table-hover">
        <thead>
          <tr>
            <th class="text-center" rowspan="2">No</th>
            <th class="text-center" rowspan="2">Jenis Alat</th>
            <th class="text-center" colspan="3">Last Record</th>
          </tr>
          <tr>
            <th class="text-center">Tanggal</th>
            <th class="text-center">Jam</th>
            <th class="text-center">Record</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $k = 0;
          $s = 0;
          foreach ($all_alat->result() as $row) {
            $first_id = $row->id_alat[0];
            if($first_id=='A'){
              $tanggal = $this->lokasi->last_hujan($row->id_lokasi)->row()->tanggal;
              $jam = $this->lokasi->last_hujan($row->id_lokasi)->row()->jam;
              $record = $this->lokasi->last_hujan($row->id_lokasi)->row()->curah_hujan . "mm";
            }elseif($first_id=='G'){
              $tanggal = $this->lokasi->last_getaran($row->id_lokasi)->row()->tanggal;
              $jam = $this->lokasi->last_getaran($row->id_lokasi)->row()->jam;
              $record = $this->lokasi->last_getaran($row->id_lokasi)->row()->frequensi . "Hertz";
            }elseif($first_id=='K'){
              $tanggal = $this->lokasi->last_kelembaban($row->id_lokasi)->row($k)->tanggal;
              $jam = $this->lokasi->last_kelembaban($row->id_lokasi)->row($k)->jam;
              $record = $this->lokasi->last_kelembaban($row->id_lokasi)->row($k)->kelembaban . "%";
              $k++;
            }elseif($first_id=='S'){
              $tanggal = $this->lokasi->last_suhu($row->id_lokasi)->row($s)->tanggal;
              $jam = $this->lokasi->last_suhu($row->id_lokasi)->row($s)->jam;
              $record = $this->lokasi->last_suhu($row->id_lokasi)->row($s)->suhu . "&nbsp;&deg;C";
              $s++;
            }
            ?>
            <tr>
              <td class="text-center"><?=$no;?></td>
              <td><?=$row->nama_alat;?> <span class="text-info">(<?=$row->id_alat;?>)</span></td>
              <td class="text-center" nowrap=""><?=$tanggal;?></td>
              <td class="text-center" nowrap=""><?=$jam;?></td>
              <td class="text-center" nowrap=""><?=$record;?></td>
            </tr>
            <?php
            $no++;
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal">
      <i class="ti-close"></i>
    Close</button>
  </div>
</div>