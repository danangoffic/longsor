<!-- CONTAINER CONTENT -->
<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title"><?=$subtitle;?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        <li><a href="<?=site_url();?>">Dashboard</a></li>
        <li><a href="#"><?=$title;?></a></li>
        <li class="active"><?=$subtitle?></li>
      </ol>
    </div>

  </div>

  <div class="row">
    <div class="col-lg-12">
      <p class="text-muted m-b-30"></p>
      <!-- start of alert html -->
      <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-check"></i>
      <strong>Success</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif ?>
      <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-times"></i>
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php endif ?>
      <!-- end of alert html -->
    </div>
	  	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	        <div class="panel panel-info">
	            <div class="panel-heading"><?=$subtitle;?></div>
	            <div class="panel-wrapper collapse in">
	                <div class="panel-body">
	                	<?=form_open('lokasi/update', array('class'=>'form-horizontal'))?>
                      <div class="form-group">
                        <label class="col-md-4 label-control" for="id_lokasi">ID Lokasi</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="id_lokasi" id="id_lokasi" placeholder="ID Lokasi" data-title="ID Lokasi" readonly="" value="<?=$lokasi->id_lokasi;?>">
                          <input type="hidden" name="id" value="<?=$lokasi->id_lokasi;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-4 label-control" for="lokasi">Lokasi</label>
                        <div class="col-md-8">
                          <input type="text" name="lokasi" autofocus="" class="form-control" id="lokasi" placeholder="Nama Lokasi"  data-title="Nama Lokasi" required="" value="<?=$lokasi->lokasi;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-4 label-control" for="lat">Koordinat</label>
                        <div class="col-md-4">
                          <input type="text" name="lat" class="form-control" id="lat" placeholder="Koordinat Latitude" data-title="Koordinat Latitude" value="<?=$lokasi->lat;?>">
                        </div>
                        <div class="col-md-4">
                          <input type="text" name="lon" class="form-control" id="lon" placeholder="Koordinat Longitude" data-title="Koordinat Longitude" value="<?=$lokasi->lon;?>">
                        </div>
                      </div>
	                </div>
                  <div class="panel-footer">
                    <div class="form-group">
                      <div class="pull-right">
                        <a class="btn btn-inverse waves-effect waves-light text-white" href="<?=base_url('lokasi')?>">
                          <i class="ti-arrow-left"></i>
                          Kembali
                        </a>
                        <button type="submit" class="btn btn-success waves-effect waves-light text-white">
                          <i class="ti-save"></i>
                          Simpan
                        </button>
                      </div>
                    </div>
                  </div>
                  <?=form_close();?>
	            </div>
	      	</div>
	  	</div>

      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="panel panel-info">
          <div class="panel-heading">Map</div>
          <div class="panel-wrapper collapse in" aria-exanded="true">
            <div class="panel-body">
              <div id="mapp" style="width: 100% !important; height: 350px !important;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
<!-- END CONTAINER CONTENT -->
