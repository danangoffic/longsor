<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_alat extends CI_Model
{
	public function all()
	{
		$this->db->select('a.id_alat, a.nama_alat, l.id_lokasi, l.lokasi, a.lat, a.lon');
		$this->db->from('data_alat a');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->order_by('id_alat', 'ASC');
		return $this->db->get();
	}

	public function by_id($id)
	{
		$this->db->select('a.id_alat, a.nama_alat, l.id_lokasi, l.lokasi, a.lat, a.lon');
		$this->db->from('data_alat a');
		$this->db->join('data_lokasi l', 'a.id_lokasi = l.id_lokasi');
		$this->db->where('id_alat', $id);
		return $this->db->get();
	}

	public function insert($data)
	{
		return $this->db->insert('data_alat', $data);
	}

	public function update($id, $data)
	{
		$this->db->where('id_alat', $id);
		return $this->db->update('data_alat', $data);
	}

	public function delete($id)
	{
		$this->db->where('id_alat', $id);
		return $this->db->delete('data_alat');
	}

	public function get_subs_a($id)
	{
		$this->db->like('id_alat', $id, 'after');
		$this->db->select('SUBSTRING(id_alat,1,3) AS id, SUBSTRING(id_alat,6) AS last_num, nama_alat, id_alat');
		$this->db->from('data_alat');
		$this->db->order_by('id_alat', 'DESC');
		$this->db->limit(1);
		return $this->db->get();
	}

	public function get_subs($id)
	{
		$this->db->like('id_alat', $id, 'after');
		$this->db->select('SUBSTRING(id_alat,1,1) AS id, SUBSTRING(id_alat,2) AS last_num, nama_alat, id_alat');
		$this->db->from('data_alat');
		$this->db->order_by('id_alat', 'DESC');
		$this->db->limit(1);
		return $this->db->get();
	}

	public function check_sub($id)
	{
		$this->db->where('id_alat', $id);
		$this->db->select('id_alat, id_lokasi, nama_alat, lat, lon');
		$this->db->from('data_alat');
		$this->db->limit(1);
		return $this->db->get();
	}
}