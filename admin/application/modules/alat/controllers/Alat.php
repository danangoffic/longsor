<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Alat extends Base_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('Model_alat', 'alat');
    $this->load->model('lokasi/Model_lokasi', 'lokasi');

    if (!$this->session->userdata('username')) {
      redirect('user/login');
    }
  }

  public function index() {
  	if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $page_variable = array(
            'title'     => 'Alat',
            'subtitle' => 'Data Alat',
            'modul'  => 'alat',
            'view'      =>'index',
            'alat' => $this->alat->all(),
            'script_spesific' => 'script_index'
        );

        return $this->view($page_variable);
  }

  public function add()
  {
    if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $page_variable = array(
            'title'     => 'Alat',
            'subtitle' => 'Add Alat',
            'modul'  => 'alat',
            'view'      =>'add',
            'lokasi' => $this->lokasi->all(),
            'script_spesific' => 'script_add'
        );

        return $this->view($page_variable);
  }

  public function edit($id)
  {
    if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $id_lokasi = $this->alat->by_id($id)->row()->id_lokasi;
    $page_variable = array(
            'title'     => 'Alat',
            'subtitle' => 'Edit Alat '.$id,
            'modul'  => 'alat',
            'view'      =>'edit',
            'alat' => $this->alat->by_id($id)->row(),
            'lokasi' => $this->lokasi->all()
            // 'script_spesific' => 'script_add'
        );

    return $this->view($page_variable);
  }

  public function save()
  {
    $this->form_validation->set_rules('jenis', 'Jenis Alat', 'required');
    $this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
    if (!$this->form_validation->run()) {
        $this->session->set_flashdata('error', validation_errors());
        redirect_back();
    }
    $id = $this->input->post('jenis');
    $buffer = json_encode(['key'=>session_id(), 'id'=>$id]);
    if($this->channel->send($buffer)){

    }
    if($id=='ARR'){
        $subs = $this->alat->get_subs_a($id)->row();
        $mid = '00';
        $last = intval($subs->last_num)+1;
        $name = "Rain Gauge Meter";
        $id_alat = $id . $mid . $last;
      }elseif($id=="G"||$id=="K"||$id=="S"){
        if($id=="G"){
          $name = "Sensor Getaran Tanah";
        }elseif($id=="K"){
          $name = "Sensor Kelembaban Tanah";
        }elseif($id=="S"){
          $name = "Sensor Suhu Tanah";
        }
        $total_sub = $this->alat->get_subs($id)->num_rows();
        if($total_sub !== 0){
          $subs = $this->alat->get_subs($id)->row();
          if($subs){
            $last = intval($subs->last_num)+1;
            $id_alat = $id . $last;
          }
        }else{
            $id_alat = $id . '1';
          }
      }
    $data = array(
        'id_alat' => $id_alat,
        'id_lokasi' => $this->input->post('lokasi'),
        'nama_alat' => $name,
        'lat' => $this->input->post('lat'),
        'lon' => $this->input->post('lon')
      );
    $insert = $this->alat->insert($data);
    if($insert){
      $this->session->set_flashdata('success', 'Data Alat Berhasil Disimpan');
      redirect('alat');
    }else{
      $this->session->set_flashdata('error', 'Gagal Menyimpan Data Alat');
      redirect_back();
    }
  }

  public function update()
  {
    $this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
    if (!$this->form_validation->run()) {
        $this->session->set_flashdata('error', validation_errors());
        redirect_back();
    }
    $id = $this->input->post('id_alat');
    $data = array(
        'id_lokasi' => $this->input->post('lokasi'),
        'lat' => $this->input->post('lat'),
        'lon' => $this->input->post('lon')
      );
    $update = $this->alat->update($id,$data);
    if($update){
      $this->session->set_flashdata('success', 'Data Alat Berhasil Disimpan');
      redirect('alat');
    }else{
      $this->session->set_flashdata('error', 'Gagal Menyimpan Data Alat');
      redirect('alat/edit/'.$id);
    }
  }
  public function delete($id)
  {
    $delete = $this->alat->delete($id);
    if($delete){
      $this->session->set_flashdata('success', 'Data Alat Berhasil Di Hapus');
      redirect('alat');
    }else{
      $this->session->set_flashdata('error', 'Gagal Menghapus Data Alat');
      redirect('alat');
    }
  }
}
