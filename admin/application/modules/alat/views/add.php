<!-- CONTAINER CONTENT -->
<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title"><?=$subtitle;?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        <li><a href="<?=site_url();?>">Dashboard</a></li>
        <li><a href="#"><?=$title;?></a></li>
        <li class="active"><?=$subtitle?></li>
      </ol>
    </div>

  </div>

  <div class="row">
    <div class="col-sm-12">
      <p class="text-muted m-b-30"></p>
    </div>
      <!-- start of alert html -->
      <div class="col-lg-12">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-check"></i>
      <strong>Success</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif ?>
      <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-times"></i>
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php endif ?>
      </div>
      <!-- end of alert html -->
		  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		        <div class="panel panel-info">
		            <div class="panel-heading"><?=$subtitle;?></div>
		            <div class="panel-wrapper collapse in">
		                <div class="panel-body">
		                	<?=form_open('alat/save', 'class="form-horizontal"');?>
                      <div class="form-group">
                        <label class="col-md-4 label-control">Jenis Alat</label>
                        <div class="col-md-8">
                          <select class="form-control" name="jenis" id="jenis" required="">
                            <option disabled="" selected="" value="">Pilih Jenis Alat</option>
                            <option value="ARR" label="Curah Hujan">Curah Hujan</option>
                            <option value="G" label="Getaran Tanah">Getaran Tanah</option>
                            <option value="K" label="Kelembaban Tanah">Kelembaban Tanah</option>
                            <option value="S" label="Suhu Tanah">Suhu Tanah</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-4 label-control">Lokasi</label>
                        <div class="col-md-8">
                          <select class="form-control" name="lokasi" id="lokasi" required="">
                            <option disabled="" selected="" value="">Pilih Lokasi</option>
                            <?php 
                            foreach ($lokasi->result() as $row) {
                              ?>
                              <option value="<?=$row->id_lokasi;?>" label="<?=$row->lokasi;?>"><?=$row->lokasi;?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-4 label-control">Koordinat</label>
                        <div class="col-md-4 m-b-5">
                          <input class="form-control" name="lat" id="lat" placeholder="Koordinat Latitude">
                        </div>
                        <div class="col-md-4">
                          <input class="form-control" name="lon" id="lon" placeholder="Koordinat Longitude">
                        </div>
                      </div>
                      
		                </div>
                    <div class="panel-footer">
                      <div class="form-group">
                        <div class="pull-right">
                          <a href="<?=base_url('alat');?>" class="btn btn-inverse text-white waves-effect waves-light" data-toggle="tooltip" data-title="Kembali">
                            <i class="ti-arrow-left"></i>
                            Kembali
                          </a>
                          <button class="btn btn-success text-white waves-effect waves-light" type="submit" data-toggle="tooltip" data-title="Simpan">
                            <i class="ti-save"></i>
                            Simpan
                          </button>
                        </div>
                      </div>
                      <?=form_close();?>
                    </div>
		            </div>
		      	</div>
		  	</div>
    </div>
</div>
</div>
<!-- END CONTAINER CONTENT -->
