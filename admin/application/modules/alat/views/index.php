<!-- CONTAINER CONTENT -->
<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title"><?=$subtitle;?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    	<a class="btn btn-rounded btn-info btn-outline pull-right m-l-5" href="<?=base_url('alat/add')?>">Tambah <?=$title?></a>
      <ol class="breadcrumb">
        <li><a href="<?=site_url();?>">Dashboard</a></li>
        <li><a href="#"><?=$title;?></a></li>
        <li class="active"><?=$subtitle?></li>
      </ol>
    </div>

  </div>

  <div class="row">
    <div class="col-lg-12">
      <p class="text-muted m-b-30"></p>
      <!-- start of alert html -->
    </div>  
      <!-- end of alert html -->
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php if ($this->session->flashdata('success')): ?>
      <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">
            ×
        </button>
        <i class="fa-fw fa fa-check"></i>
        <strong>Success</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
      <?php endif ?>
      <?php if ($this->session->flashdata('error')): ?>
      <div class="alert alert-danger fade in">
        <button class="close" data-dismiss="alert">
            ×
        </button>
        <i class="fa-fw fa fa-times"></i>
        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
      <?php endif ?>
    
    </div>
    <div class="col-lg-12">
		        <div class="panel panel-info">
		            <div class="panel-heading"><?=$subtitle;?></div>
		            <div class="panel-wrapper collapse in">
		                <div class="panel-body">
                      <div class="table-responsive">
  		                	<table id="dt_basic" class="table table-condensed table-bordered table-hover" width="100%">
          								<thead>
          									<tr>
          										<th class="text-center" nowrap="">No</th>
          										<th class="text-center" nowrap="">Nama Alat</th>
                              <th class="text-center" nowrap="">Lokasi</th>
          										<th class="text-center" nowrap="">Aksi</th>
          									</tr>
          								</thead>
          								<tbody>
          									<?php if($alat->num_rows() !== 0) {
          										$num = 1;
                              foreach ($alat->result() as $row) : ?>
          										<tr>
          											<td class="text-center"><?= $num; ?></td>
          											<td><?= $row->nama_alat;?>&nbsp;<span class="text-info" title="ID Alat: <?=$row->id_alat;?>">(<?=$row->id_alat;?>)</span></td>
                                <td><?=$row->lokasi;?></td>
          											<td nowrap="" class="text-center">
          												<a href="<?= base_url('alat/edit/'.$row->id_alat); ?>" class="btn btn-sm btn-info btn-circle text-white" data-toggle="tooltip" data-title="Edit &quot;<?=$row->id_alat;?>&quot;"><i class="fa fa-pencil"></i></a>
          													<a href="<?= base_url('alat/delete/'.$row->id_alat); ?>" data-toggle="tooltip" data-title="Delete &quot;<?=$row->id_alat;?>&quot;" class="btn btn-sm btn-danger btn-circle text-white" onclick="return confirm('Are you sure you want to delete this data?')"><i class="fa fa-trash"></i></a>
          											</td>
          										</tr>
          										<?php $num++; endforeach;
          									} ?>
          								</tbody>
          							</table>
                      </div>
		                </div>
		            </div>
		      	</div>
          </div>
		  	</div>
    </div>
</div>
</div>
<!-- END CONTAINER CONTENT -->
