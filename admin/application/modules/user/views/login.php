<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?= base_url() ?>assets/images/logo/logo.png" type="image/x-icon">
        <link rel="icon" href="<?= base_url() ?>assets/images/logo/logo.png" type="image/x-icon">
        <title>Aplikasi Monitoring Longsor</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url() ?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="<?php echo base_url() ?>assets/css/colors/blue.css" id="theme" rel="stylesheet">


        <style>
            .login-register{background:linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 75%) center center/cover no-repeat!important;height:100%;position:fixed;left:0;right:0}
        </style>    
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="login-register">
            <div class="login-box">
                <div class="white-box">
                    <?= form_open(base_url('user/signin'), array('id' => 'login-form')) ?> 
                    <div class="form-horizontal form-material" id="loginform">
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        <h3 class="box-title m-b-20"></h3>
                        <?php if ($this->session->flashdata('success')): ?>
                            <div class="alert alert-success fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                <i class="fa-fw fa fa-check"></i>
                                <strong>Success</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>
                        <?php endif ?>
                        <?php if ($this->session->flashdata('error')): ?>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                <i class="fa-fw fa fa-times"></i>
                                <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                            </div>
                        <?php endif ?>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input type="text" name="username" class="form-control" placeholder="Enter your username">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="password" name="password" id="login_password"  class="form-control" placeholder="Enter your password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox checkbox-primary pull-left p-t-0">
                                    <input id="checkbox-signup" type="checkbox" name="remember_me" value="1" <?php echo set_checkbox('remember_me', 1); ?>/>
                                    <label for="checkbox-signup"> Remember me </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox checkbox-primary pull-left p-t-0">
                                    <input id="checkbox-password" type="checkbox" onclick="checkAddress(this)"/>
                                    <label for="checkbox-password"> <i class="fa fa-eye"></i> </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button name="login_user" value="Submit" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                            
                        </div>
                        <div class="form-group text-center m-t-5">
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <a href="<?=base_url('../');?>">
                                    <i class="ti-arrow-left"></i>
                                    Kembali Halaman User</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">

                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <!--<p>Supported by <a href="https://iamfis.com" target="_blank">IamFIS</a></p>-->
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
        </section>
        <!-- jQuery -->
        <script src="<?php echo base_url() ?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url() ?>assets/bootstrap/dist/js/tether.min.js"></script>
        <script src="<?php echo base_url() ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="<?php echo base_url() ?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url() ?>assets/js/custom.min.js"></script>
        <!--Style Switcher -->
        <script src="<?php echo base_url() ?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script>
                                        function checkAddress(checkbox)
                                        {
                                            if (checkbox.checked)
                                            {
                                                $('#login_password').prop('type', 'text');
                                            } else {
                                                $('#login_password').prop('type', 'password');
                                            }
                                        }
        </script>
        <script type="text/javascript">
            runAllForms();

            $(function () {
                // Validation
                $("#login-form").validate({
                    // Rules for form validation
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 3,
                            maxlength: 20
                        }
                    },
                    // Messages for form validation
                    messages: {
                        email: {
                            required: 'Please enter your email address',
                            email: 'Please enter a VALID email address'
                        },
                        password: {
                            required: 'Please enter your password'
                        }
                    },
                    // Do not change code below
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }
                });
            });
        </script>
    </body>

</html>
