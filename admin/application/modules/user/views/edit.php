<!-- CONTAINER CONTENT -->
<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title"><?=$subtitle;?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    	<a class="btn btn-rounded btn-info btn-outline pull-right m-l-10" href="<?=base_url('user/add')?>">Tambah <?=$title?></a>
      <ol class="breadcrumb">
        <li><a href="<?=site_url().'trackingv2/';?>">Dashboard</a></li>
        <li><a href="#"><?=$title;?></a></li>
        <li class="active"><?=$subtitle?></li>
      </ol>
    </div>

  </div>

  <div class="row">
    <div class="col-sm-12">
      <p class="text-muted m-b-30"></p>
      <!-- start of alert html -->
      <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-check"></i>
      <strong>Success</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif ?>
      <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger fade in">
      <button class="close" data-dismiss="alert">
          ×
      </button>
      <i class="fa-fw fa fa-times"></i>
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php endif ?>
      <!-- end of alert html -->
		  	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		        <div class="panel panel-info">
		            <div class="panel-heading"><?=$subtitle;?></div>
		            <div class="panel-wrapper collapse in">
		                <div class="panel-body">
		                	<form action="" method="">
                        <div class="form-group">
                          <label class="label-control" for="username">Username:</label>
                          <input class="form-control" name="username" id="username" value="<?=$user->username;?>" readonly>
                        </div>
                        
                      </form>
		                </div>
		            </div>
		      	</div>
		  	</div>
        
      
      
        </div>
    </div>
</div>
</div>
<!-- END CONTAINER CONTENT -->
