<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
	public function all()
	{
		return $this->db->get('user');
	}

	public function by_user($username, $password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		return $this->db->get('user');
	}

	public function by_id($id)
	{
		$this->db->where('id_user',$id);
		return $this->db->get('user');
	}

	public function insert($data)
	{
		return $this->db->insert('user',$data);
	}

	public function update($id,$data)
	{
		$this->db->where('id_user',$id);
		return $this->db->update('user', $data);
	}

	public function delete($id)
	{
		$this->db->where('id_user',$id);
		return $this->db->delete('user');
	}
}