<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Base_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('User_model','user');
  }

  public function index() {
  	if(!$this->session->userdata('username')) {
            redirect(base_url());
        }
    $page_variable = array(
            'user' => $this->user->all(),
            'modul' => 'user',
            'title'     => 'User',
            'subtitle' => 'Data User',
            'view'      =>'index',
            'script_spesific' => 'script_index'
        );

        return $this->view($page_variable);
  }

  public function login()
  {
    $page_variable = array(
  'title' => 'Login',
    );
        return $this->load->view('login', $page_variable);
  }

  public function signin()
  {
    $username = strtolower($this->input->post('username'));
    $password = md5($this->input->post('password'));
    if($this->user->by_user($username,$password)->num_rows()!==0){
      $login = $this->user->by_user($username, $password)->row();
      $this->session->set_userdata('username', $username);
      $this->session->set_userdata('nama', $login->nama);
      $this->session->set_userdata('id_user', $login->id_user);
      redirect(base_url());
    }else{
      $this->session->set_flashdata("error", "The username and password you entered don't match");
      redirect_back();
    }
  }

  public function logout()
  {
    if ($this->uri->segment(2) !== '' && $this->uri->segment(3) !== '') {
      $this->session->sess_destroy();
    }
    redirect(base_url());
  }

  public function add()
  {
    # code...
  }

  public function edit($id)
  {
    # code...
  }
}
